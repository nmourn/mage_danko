#!/bin/sh

# Update 191 and 172 folders to use the most recent theme files.

current_dir=${PWD##*/}
PROJECT_PATH="/var/www/sites/magento/mage_hipster"

if [ $current_dir != "mage_hipster" ]
    then
    cd $PROJECT_PATH
fi

case "$1" in
'172')
    printf "Extracting core theme files to 1.7.2 ...\n"
    ./archive 172
    cd 172/
    mv ../out/danko172.zip .
    unzip -qo danko172.zip
    rm danko172.zip
    ;;
'191')
    printf "Extracting core theme files to 1.9.1 ...\n"
    ./archive 191
    cd 191/
    mv ../out/danko191.zip .
    unzip -qo danko191.zip
    rm danko191.zip
    ;;
*)
    printf "Provide magento version (172 or 191)...\n"
    ;;
esac
