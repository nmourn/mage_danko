#!/usr/bin/python
"""
Get all strings passed to magento translation function 
and print in form "string","string" allowing to use as csv file
Input: space separated list of filepaths
Output: strings prepared to translation in magento in form "string","string" 
        printed to stdout.
"""

import re, sys, StringIO, csv

def get_strings(fname):
    "get all translatable strings from file fname as set of strings"
    strings = set([])
    with open(fname) as f:
        for line in f:
            res = re.findall(r"\$this->__\('(.*?)'\)", line)
            for match in res:
                strings.add(match)
    return strings

def print_csv(string_list):
    "print list of strings encoded in magento csv format"
    output = StringIO.StringIO()
    csv_writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
    for string in string_list:
        csv_writer.writerow([string, string])
    content = output.getvalue()
    print content
    output.close()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        filenames = sys.argv[1:]
        strs = set([])
        for filename in filenames:
            strs = strs.union(get_strings(filename))
        strs = sorted(list(strs))
        print_csv(strs)
    else:
        print "Provide filenames separated by spaces"
