#!/bin/bash

# The main goal of this hook:
# if the commit changes some file in 181 folder that is part of a patch (172 or 191)
# then we should check whether we also updated (in the same commit) corresponding file in patch folder 
# if not - commit fails with error message
# First it checks whether files changed by current commit in core (181) folder are part of other patch
# if so, it checks whether files from patches are also added in current commit. 
# Therefore, change to the file in core folder should be in the same commit as the corresponding change in patch

# files scripts/data/list172 and scripts/data/list191 are used to keep track of patch files
# so the script also updates these files whenever we add new file in the patch (by editing and commiting it)

# strip first folder from path: foo/bar/buzz -> bar/buzz
function strip_first_folder {
    cut -f 1 -d '/' --complement $1
}

# get sorted list of changed files (from the start of the version) in some version, relative to version's folder
# provide version (eg 191, 172) as an argument
function get_changed_files {
    git diff --name-only --relative --diff-filter=ACMRTUXB start$1 HEAD | grep $1 | strip_first_folder | sort
}

# get sorted list of files, changed  by current commit in some version, relative to version's folder
# provide version as an argument
function get_changed_commit {
    git diff --cached --name-only | grep $1 | strip_first_folder | sort
}

# cd ..
# get_changed_commit 181
# exit

# go to repository root directory
cd $(git rev-parse --show-toplevel)
# cleanup whatever remained from previous checks
rm tmp/*

# if commit touches something in 181 that is in file-lists of 172 or 191

# get list of changed files for every version separately
get_changed_commit 181 > tmp/commit_files181
get_changed_commit 172 > tmp/commit_files172
get_changed_commit 191 > tmp/commit_files191

# get intersection between current commit core files and patch files
grep -F -f tmp/commit_files181 scripts/data/list191 | sort > tmp/intersect191
grep -F -f tmp/commit_files181 scripts/data/list172 | sort > tmp/intersect172
# find files that are missing from patch
grep -F -v -f tmp/commit_files191 tmp/intersect191 | sort > tmp/missing191
grep -F -v -f tmp/commit_files172 tmp/intersect172 | sort > tmp/missing172

if [ "$(cat tmp/missing191)" ]
then
    echo "There are files that have been changed in core but not in version 1.9.1:"
    cat tmp/missing191
    FAILED="1"
fi

if [ "$(cat tmp/missing172)" ]
then
    echo "There are files that have been changed in core but not in version 1.7.2:"
    cat tmp/missing172
    FAILED="1"
fi

# if environment variable set then make commit anyway
# otherwise, exit
if [ $FORCE_COMMIT ]
then
    FAILED="0"
fi

if [ "$FAILED" = "1" ]
then
    echo "exiting"
    exit 1
fi

## if commit touches something in 172 or 191 then update their file-lists

# if this commit touches something in 172 directory
if [ "$(git diff --cached --name-only | grep 172)" ]
then
    echo "Updating list of changed files in patch 1.7.2"
    get_changed_files 172 > scripts/data/list172
fi

# if this commit touches something in 191 directory
if [ "$(git diff --cached --name-only | grep 191)" ]
then
echo "Updating list of changed files in patch 1.9.1"
    get_changed_files 191 > scripts/data/list191
fi
