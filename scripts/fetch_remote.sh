#!/bin/bash
# update local with files from remote, that are located at $PROJECT/in/

current_dir=${PWD##*/}
PROJECT_PATH="/var/www/sites/magento/mage_hipster"


if [ $current_dir != "mage_hipster" ]
    then
    cd $PROJECT_PATH
fi

cd in
rm -rf *
echo "fetching remote archive..."
curl http://owl-themes.net/test/danko/run.php
wget http://owl-themes.net/test/danko/191.zip
cd ..

# update 191 folder with all files from remote
echo "updating 191..."
cp in/191.zip 191/
cd 191/
unzip -qo 191.zip
rm 191.zip

# update 181 folder with all files from remote except for the files that are changed in 191
echo "updating 181..."
cd $PROJECT_PATH
cd in/
unzip -qo 191.zip
# remove all files that are changed in 191
cat ../scripts/data/list191 | xargs rm -rf
rm 191.zip
# update 181 with what remains
rsync -r * ../181/

