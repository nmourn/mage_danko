#!/bin/sh

current_dir=${PWD##*/}
PROJECT_PATH="/var/www/sites/magento/mage_hipster"

if [ $current_dir != "mage_hipster" ]
    then
    cd $PROJECT_PATH
fi

rm -rf out/*
printf "Making core archive...\n"
cd 181/
git diff --name-only --relative init HEAD . | xargs zip -rq danko.zip
mv danko.zip ../out
printf "done!\n"
cd $PROJECT_PATH

case "$1" in
'191')
    printf "Making 1.9.1 patch...\n"
    cd 191/
    git diff --name-only --relative --diff-filter=ACMRTUXB start191 HEAD . | xargs zip -rq danko191-patch.zip
    mv danko191-patch.zip ../out
    cd ../out/
    printf "Merging core and patch together\n"
    unzip -qo danko.zip && rm danko.zip
    unzip -qo danko191-patch.zip && rm danko191-patch.zip
    zip -qr danko191.zip *
    printf "done!\n"
    ;;
'172')
    printf "Making 1.7.2 patch...\n"
    cd 172/
    git diff --name-only --relative --diff-filter=ACMRTUXB start172 HEAD . | xargs zip -rq danko172-patch.zip
    mv danko172-patch.zip ../out
    cd ../out/
    printf "Merging core and patch together\n"
    unzip -qo danko.zip && rm danko.zip
    unzip -qo danko172-patch.zip && rm danko172-patch.zip
    zip -qr danko172.zip *
    printf "done!\n"
    ;;
esac
