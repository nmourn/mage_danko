<?php

class OwlThemes_Danko_Helper_Mediafallback extends Mage_ConfigurableSwatches_Helper_Mediafallback {

     public function getConfigurableImagesFallbackArray(Mage_Catalog_Model_Product $product, array $imageTypes, $w, $h,
        $keepFrame = false
    ) {
        // !!! probably move move to parameters
        $rw = $w * 2;
        $rh = $w * 2;
        if (!$product->hasConfigurableImagesFallbackArray()) {
            $mapping = $product->getChildAttributeLabelMapping();

            $mediaGallery = $product->getMediaGallery();

            if (!isset($mediaGallery['images'])) {
                return array(); //nothing to do here
            }

            // ensure we only attempt to process valid image types we know about
            $imageTypes = array_intersect(array('image', 'small_image'), $imageTypes);

            $imagesByLabel = array();
            $imageHaystack = array_map(function ($value) {
                return Mage_ConfigurableSwatches_Helper_Data::normalizeKey($value['label']);
            }, $mediaGallery['images']);

            // load images from the configurable product for swapping
            foreach ($mapping as $map) {
                $imagePath = null;

                //search by store-specific label and then default label if nothing is found
                $imageKey = array_search($map['label'], $imageHaystack);
                if ($imageKey === false) {
                    $imageKey = array_search($map['default_label'], $imageHaystack);
                }

                //assign proper image file if found
                if ($imageKey !== false) {
                    $imagePath = $mediaGallery['images'][$imageKey]['file'];
                }

                $imagesByLabel[$map['label']] = array(
                    'configurable_product' => array(
                        Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_SMALL => null,
                        Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_SMALL . "_retina"=> null,
                        Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE => null,
                        Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE . "_retina" => null,
                    ),
                    'products' => $map['product_ids'],
                );

                if ($imagePath) {
                    $imagesByLabel[$map['label']]['configurable_product']
                        [Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_SMALL] =
                            $this->_resizeProductImage($product, 'small_image', $w, $h, $keepFrame, $imagePath);

                    $imagesByLabel[$map['label']]['configurable_product']
                        [(Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_SMALL) . "_retina"] =
                            $this->_resizeProductImage($product, 'small_image', $rw, $rh, $keepFrame, $imagePath);

                    $imagesByLabel[$map['label']]['configurable_product']
                        [Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE] =
                            $this->_resizeProductImage($product, 'image', $w, $h, $keepFrame, $imagePath);

                    $imagesByLabel[$map['label']]['configurable_product']
                        [(Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE) . "_retina"] =
                            $this->_resizeProductImage($product, 'image', $rw, $rh, $keepFrame, $imagePath);
                }
            }

            $imagesByType = array(
                'image' => array(),
                'image_retina' => array(),
                'small_image' => array(),
                'small_image_retina' => array(),
            );

            // iterate image types to build image array, normally one type is passed in at a time, but could be two
            foreach ($imageTypes as $imageType) {
                // load image from the configurable product's children for swapping
                /* @var $childProduct Mage_Catalog_Model_Product */
                if ($product->hasChildrenProducts()) {
                    foreach ($product->getChildrenProducts() as $childProduct) {
                        if ($image = $this->_resizeProductImage($childProduct, $imageType, $w, $h, $keepFrame)) {
                            $imagesByType[$imageType][$childProduct->getId()] = $image;
                            $imageRetina = $this->_resizeProductImage($childProduct, $imageType, $rw, $rh, $keepFrame);
                            $imagesByType[$imageType . "_retina"][$childProduct->getId()] = $imageRetina;
                        }
                    }
                }

                // load image from configurable product for swapping fallback
                if ($image = $this->_resizeProductImage($product, $imageType, $w, $h, $keepFrame, null, true)) {
                    $imagesByType[$imageType][$product->getId()] = $image;
                    $imageRetina = $this->_resizeProductImage($product, $imageType, $rw, $rh, $keepFrame, null, true);
                    $imagesByType[$imageType . "_retina"][$product->getId()] = $imageRetina;
                }
            }

            $array = array(
                'option_labels' => $imagesByLabel,
                Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_SMALL => $imagesByType['small_image'],
                Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE => $imagesByType['image'],
                Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_SMALL . "_retina" => $imagesByType['small_image_retina'],
                Mage_ConfigurableSwatches_Helper_Productimg::MEDIA_IMAGE_TYPE_BASE . "_retina"=> $imagesByType['image_retina'],
            );

            $product->setConfigurableImagesFallbackArray($array);
        }

        return $product->getConfigurableImagesFallbackArray();
    }

    protected function _resizeProductImage($product, $type, $w, $h, $keepFrame, $image = null, $placeholder = false)
    {
        $hasTypeData = $product->hasData($type) && $product->getData($type) != 'no_selection';
        if ($image == 'no_selection') {
            $image = null;
        }
        if ($hasTypeData || $placeholder || $image) {
            $helper = Mage::helper('catalog/image')
                ->init($product, $type, $image)
                ->keepFrame(($hasTypeData || $image) ? $keepFrame : false)  // don't keep frame if placeholder
            ;
            $helper->constrainOnly(true)->resize($w, $h);
            return (string)$helper;
        }
        return false;
    }
}

