/*
Resizer allows to hook custom functions to window resizing in similar fashion
css media queries work: provide a breakpoint and a function to be fired.
Width of window is mapped to some breakpoint, when window is resized from one breakpoint to another,
function associated with new breakpoint is fired
When resizing within one breakpoint no action is performed
conf: object where
	keys: width breakpoints
	values: callback functions to be executed when certain breakpoint is reached
*/
function Resizer(conf) {

	var resizer = (function ($) {
	
		return {

			// list of all breakpoints
			bpPool: [],
			// old and new values storage
			values: {},
			// callbacks to be executed for each breakpoint
			callbacks: {},

			/*
			initialize resizer using config object
			*/
			init: function (config) {
				var self = this;
				for (var key in config) {
					self.bpPool.push(+key);
					self.callbacks[+key] = config[key];
				};
				self.bpPool.sort(function(a,b) {return a-b});
				var initialBreakpoint = self.closestBp($(window).width());
				self.values['old'] = initialBreakpoint;
				self.values['new'] = initialBreakpoint;
				self._runOnBreakpoint(initialBreakpoint);
			},

			/*
			return key k in bpPool, such that k is smallest for all k >= w
            or max in bpPool if w > k
			*/
			closestBp: function (w) {
				var self = this;
				for (var i = 0; i < self.bpPool.length; ++i) {
					if (self.bpPool[i] >= w)
						return self.bpPool[i];
				}
				return self.bpPool.max();
			},

			/* Check if given breakpoint k is different from old, then run callback for the new one */
			_runOnBreakpoint: function (k) {
				var self = this;
				self.values['old'] = self.values['new'];
				self.values['new'] = k;
				// if jumped from one breakpoint to another
				if (self.values['old'] !== self.values['new'])
					self.callbacks[k]();
			},

			initEvents: function() {
				var self = this;
				$(window).resize(function (e) {
					var width = $(this).width();
					k = self.closestBp(width);
					self._runOnBreakpoint(k);
				});
			}
		}
	})(jQuery);

	resizer.init(conf);
	resizer.initEvents();
	return resizer;
}
