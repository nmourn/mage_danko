(function(jQuery){
     jQuery.fn.extend({
         categories: function() {       
            return this.each(function() { 
            	var listObject				= jQuery(this),
                    activationEffectSpeed 	= 'fast', 
					activeClassName			= 'active',
					activationEffect 		= 'slideToggle', 
                    elementDataKey			= 'accordiated',
					panelSelector			= 'ul, div', 
					itemSelector			= 'li'; 
				if(listObject.data(elementDataKey))
					return false; 
				jQuery.each(listObject.find('span.open'), function(){ 
					jQuery(this).bind('activate-node', function(){
						listObject.find( panelSelector ).not(jQuery(this).parents()).not(jQuery(this).siblings()).slideUp( activationEffectSpeed );
						activate(this,'slideDown');
					});
 					jQuery(this).click(function(e){
						activate(this, activationEffect);
						return void(0);
					});   
				});
				jQuery.each(listObject.find('ul, li>div'), function(){
					jQuery(this).data(elementDataKey, true);
					jQuery(this).hide();
				});				
				var active = (location.hash)?listObject.find('a[href=\'' + location.hash + '\']')[0]:listObject.find('li.current a')[0];

				if(active){
					activate(active, false);
				}
				
				function activate(el,effect){
					
					jQuery(el).parent( itemSelector ).siblings().removeClass(activeClassName).children( panelSelector ).slideUp( activationEffectSpeed );
					
					jQuery(el).siblings( panelSelector )[(effect || activationEffect)](((effect == "show")?activationEffectSpeed:false),function(){

						if(effect == 'show'){
							jQuery(el).parents( itemSelector ).not(listObject.parents()).addClass(activeClassName);
						}                        
                        
						if(jQuery(el).siblings( panelSelector ).is(':visible')){
							jQuery(el).parents( itemSelector ).not(listObject.parents()).addClass(activeClassName);
						} else {
							jQuery(el).parent( itemSelector ).removeClass(activeClassName);
						} 
						jQuery(el).parents().show();
					
					});
					
				}
				
            });
        }
    }); 
})(jQuery);

