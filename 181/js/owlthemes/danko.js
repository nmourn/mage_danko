var dankoTheme = (function ($) {

    return {

        _cache: {},

        init: function() {

            this.initEvents();
        },

        incrementCount: function (btn) {
            
            var btn = $(btn)
                ,input = btn.siblings('input')
                ,count = +input.attr('value');
            if (count !== NaN) {
                input.attr('value', count+1);
            }
        },

        decrementCount: function (btn) {
            
            var btn = $(btn)
                ,input = btn.siblings('input')
                ,count = +input.attr('value');
            if (count !== NaN && count > 0) {
                input.attr('value', count-1);
            }
        },

        updateTopCart: function (newContent, topElement) {
            if (topElement !== undefined) { 
                var cart = $('.block-cart-header', topElement.document);
            }
            else
                var cart = $('.block-cart-header');
            cart.replaceWith(newContent);
            topElement.truncateOptions();
        },

        ajaxCartAdd: function (url, productId) {

            var self = this
                ,ajaxUrl = url.replace('checkout/cart/', 'owlthemes/cart/')
                ,pending = $('#ajax-popup .pending')
                ,popupWindow = $('#ajax-popup .content')
                ,productSelector = ("div.product_"+productId+":first")
                ,product = $(productSelector)
                ,productName = product.find('.name').text().trim()
                ,productImgSrc = product.find('.product-image > img').attr("src");
            pending.show();
            $('#ajax-popup-open').click();
            $.ajax({
                url: ajaxUrl,
                dataType: 'json',
            })
            .done(function (data) {
                image = $('<img />').attr("src", productImgSrc);
                image.attr('width', 75).attr('height', 75);
                if (data['error'] !== undefined) {
                    popupWindow.html(data['error']);
                    return;
                }
                popupWindow.find('.product-img').html(image);
                popupWindow.find('.product-name').html(productName);
                self.updateTopCart(data['top_cart']);
            })
            .fail(function (error) {
                console.log( error );
                popupWindow.html(error.getMessage());
              })
            .always(function() {
                pending.hide();
              });
        },

        ajaxShowOptions: function (url, productId) {

            var ajaxUrl = url.replace('checkout/cart/add', 'owlthemes/cart/options')
                ,optionsLink = $('#ajax-popup-options');
            optionsLink.attr("href", ajaxUrl);
            optionsLink.click();
        },

        ajaxFormAdd: function (formId) {

            var self = this
                ,form = $('#'+formId)
                ,url = form.attr("action")
                ,pending = $('#ajax-popup .pending')
                ,popupWindow = $('#ajax-popup .content')
                ,productName = $('.product-name h1').text()
                ,productImgSrc = $('#main-thumb').attr("src");

            ajaxUrl = url.replace('checkout/cart/', 'owlthemes/cart/');
            pending.show();
            $('#ajax-popup-open').click();
            form.ajaxSubmit({
                url: ajaxUrl,
                type: 'post',
                success: function (data, statusText, xhr, $form) {
                    image = $('<img />').attr("src", productImgSrc);
                    image.attr('width', 75).attr('height', 75);
                    popupWindow.find('.product-img').html(image);
                    popupText = popupWindow.find('.result-text');
                    successText = popupText.html().replace(/{{product_name}}/, productName);
                    popupText.html(successText);
                    if (data['error'] !== undefined) {
                        popupWindow.html(data['error']);
                    }
                    else {
                        self.updateTopCart(data['top_cart']);
                    }
                },
                error: function (error) {
                    console.log( error );
                    popupWindow.html(error.getMessage());
                },
                complete: function () {
                    pending.hide();
                }
            });
        },

        ajaxOptionsSubmit: function (formId) {

            var self = this
                ,form = $('#'+formId)
                ,url = form.attr("action")
                ,btnAdd = form.find('.btn-cart span span');

            btnAdd.addClass('pending').text("pending");
            ajaxUrl = url.replace('checkout/cart/', 'owlthemes/cart/');
            form.ajaxSubmit({
                url: ajaxUrl,
                type: 'post',
                success: function (data, statusText, xhr, $form) {
                    if (data['error'] !== undefined) {
                        btnAdd.addClass('error').html(data['error']);
                    }
                    else {
                        btnAdd.text('ok');
                        self.updateTopCart(data['top_cart'], top);
                        top.jQuery.magnificPopup.close();
                    }
                },
                error: function (error) {
                    console.log( error );
                    btnAdd.addClass('error').html(error.getMessage());
                },
                complete: function () {
                    btnAdd.removeClass('pending');
                }
            });
        },

        /**
        * Init product page main image zoom
        * target - jquery object of target area
        */
        createZoom: function (target, params) {
            var self = this;
            self._cache['product_thumbnails'] = target.html();
            if (typeof params !== 'undefined' && params)
                self._cache['zoom_gallery_params'] = params;
            else
                params = self._cache['zoom_gallery_params'];
            target.etalage({
                thumb_image_width: params.thumbWidth,
                autoplay: false,
                thumb_image_height: params.thumbHeight,
                click_callback: function(image_anchor, instance_id) {
                    jQuery('a[rel="gallery"][href="'+image_anchor+'"]').trigger('click');
                },
                smallthumbs_position: params.thumbsPos,
                zoom_area_width: params.areaWidth,
                source_image_width: params.srcWidth,
                source_image_height: params.srcHeight,
            });
        },

        destroyZoom: function (target) {
            var self = this;
            target.html(self._cache['product_thumbnails']);
        },

        initEvents: function() {

            //hoverswap
            $('div.img-container')
                .on('mouseenter', '.hover-hidden', function() {
                    $(this).animate({'opacity': 0}, 200);
                })
                .on('mouseleave', '.hover-hidden', function() {
                    $(this).animate({'opacity': 1}, 200);
                });
        }
    }
})(jQuery);