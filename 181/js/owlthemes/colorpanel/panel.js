var EMColorPanel = (function () {

    var elems = [];

    function changeColor(element, value) {

        if (typeof(value) !== 'string')
            throw new Error('invalid color type');
        if (value[0] !== '#')
            value = "#"+value;
        if (value.length != 4 && value.length != 7)
            throw new Error('invalid color length: ' + value.length);
        element.html(element.html().replace(/#[a-fA-F0-9]{3,6}/g, value));
    }

    function changeUrl(element, url) {

        regex = /url\(.*\);/;
        element.html(element.html().replace(regex, "url:(" + url + ")"));
    }

    return {

        init: function (styleElements) {

            for (var k in styleElements) {
                elems[k] = styleElements[k];
            }
        },

        setActiveColor: function (value) {

            changeColor(elems['activeColor'], value);
            // changeColor(this.elems['borderColor'], value);
        },

        setSecondaryColor: function (value) {

            // blah blah
        },

        setBackgroundUrl: function (url) {

            changeUrl(elems['bgUrl'], url);
        },

    }
})();


jQuery(function() {

    EMColorPanel.init({

        'activeColor': jQuery('#active-color'),
        'bgUrl': jQuery('#bg-url'),
    })

    EMColorPanel.setActiveColor('#111111');
    EMColorPanel.setBackgroundUrl('mah-url');
})
