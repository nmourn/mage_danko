var googleFontPreviewModel = Class.create();

    googleFontPreviewModel.prototype = {
        initialize : function(fontElement, previewElement)
        {
            this.fontElement = $(fontElement);
            this.previewElement = $$("div."+previewElement)[0];
            this.loadedFonts = "";

            this.refreshPreview();
            this.bindFontChange();
        },
        bindFontChange : function()
        {
            Event.observe(this.fontElement, "change", this.refreshPreview.bind(this));
            Event.observe(this.fontElement, "keyup", this.refreshPreview.bind(this));
            Event.observe(this.fontElement, "keydown", this.refreshPreview.bind(this));
        },
        refreshPreview : function()
        {
            if ( this.loadedFonts.indexOf( this.fontElement.value ) > -1 ) {
                this.updateFontFamily();
                return;
            }

            var ss = document.createElement("link");
            ss.type = "text/css";
            ss.rel = "stylesheet";
            ss.href = "http://fonts.googleapis.com/css?family=" + this.fontElement.value;
            document.getElementsByTagName("head")[0].appendChild(ss);

            this.updateFontFamily();

            this.loadedFonts += this.fontElement.value + ",";
        },
        updateFontFamily : function()
        {
            $(this.previewElement).setStyle({ fontFamily: this.fontElement.value });
        }
    }