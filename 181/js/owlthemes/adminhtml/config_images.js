var configTransformer = {

	/*
	imgs: list of urls for images in the same order options are presented
	selectId: id of select element 
	*/
	init: function (conf) {
		self = configTransformer;
		self.imgs = conf.imgs;
		self.selectId = conf.selectId;
	},

	transform: function () {
		(function ($) {
			var i = 0;
			var select = $("#"+self.selectId);
			var opts = select.find("option");
			opts.each(function (idx) {
				$this = $(this);
				$this.attr("data-imagesrc", self.imgs[i % self.imgs.length]);
				i++;
			});
			var inputName = select.attr("name");
			select.ddslick();
			// now its div, not select
			selectBlock = $("#"+self.selectId);
			selectBlock.find(".dd-options a").on("click", function (e) {
				input = jQuery(this).find("input").clone();
				// destroy old input
				selectBlock.find("> input").remove()
				// append new input
				input.attr("name", inputName);
				selectBlock.append(input);
				selectBlock.find("> input").attr("name", inputName);
			});
		})(jQuery);
	},
}