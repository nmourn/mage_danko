<?php
class OwlThemes_Navigation_Model_System_Config_Source_Menu_Type {

    public function toOptionArray() {

        return array(
            array('value' => '1', 'label' => Mage::helper('owlthemes_navigation')->__('Superfish')),
            array('value' => '2', 'label' => Mage::helper('owlthemes_navigation')->__('Wide')),
        );
    }
}
