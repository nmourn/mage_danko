<?php
class OwlThemes_Navigation_Model_System_Config_Source_Custom_Item_Hover {

    public function toOptionArray() {

        return array(
            array('value' => '0', 'label' => Mage::helper('owlthemes_navigation')->__('Disabled')),
            array('value' => '1', 'label' => Mage::helper('owlthemes_navigation')->__('CMS Block')),
            array('value' => '2', 'label' => Mage::helper('owlthemes_navigation')->__('Custom Content')),
        );
    }
}