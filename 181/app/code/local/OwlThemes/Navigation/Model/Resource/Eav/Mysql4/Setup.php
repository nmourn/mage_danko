<?php
class OwlThemes_Navigation_Model_Resource_Eav_Mysql4_Setup extends Mage_Catalog_Model_Resource_Setup {

    public function getDefaultEntities() {

        return array(
            'catalog_category' => array(
                'entity_model'                   => 'catalog/category',
                'attribute_model'                => 'catalog/resource_eav_attribute',
                'table'                          => 'catalog/category',
                'additional_attribute_table'     => 'catalog/eav_attribute',
                'entity_attribute_collection'    => 'catalog/category_attribute_collection',
                'attributes'                     => array(
                    'css_class'               => array(
                        'type'                       => 'varchar',
                        'label'                      => 'CSS classes',
                        'backend'                     => '',
                        'input'                      => 'text',
                        'required'                   => false,
                        'sort_order'                 => 10,
                        'visible'                       => true,
                        'visible_on_front'           => true,
                        'group'                         => 'OwlThemes Navigation',
                        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                    ),
                    
                    'custom_label'          => array(
                        'type'                       => 'int',
                        'label'                      => 'Category Label',
                        'input'                      => 'select',
                        'required'                   => false,
                        'source'                     => 'owlthemes_navigation/category_attribute_source_label',
                        'sort_order'                 => 20,
                        'group'                      => 'OwlThemes Navigation',
                        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                    ),
                    'custom_top_content'        => array(
                        'type'                       => 'text',
                        'label'                      => 'Custom top content',
                        'input'                      => 'textarea',
                        'required'                   => false,
                        'sort_order'                  => 30,
                        'visible'                    => true,
                        'visible_on_front'           => true,
                        'group'                         => 'OwlThemes Navigation',
                        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'is_html_allowed_on_front'   => true,
                        'wysiwyg_enabled'            => true,
                    ),
                    
                    'custom_right_content'        => array(
                            'type'                       => 'text',
                            'label'                      => 'Custom right content',
                            'input'                      => 'textarea',
                            'required'                   => false,
                               'sort_order'                  => 40,
                               'visible'                    => true,
                               'visible_on_front'           => true,
                               'group'                         => 'OwlThemes Navigation',
                               'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                               'is_html_allowed_on_front'   => true,
                               'wysiwyg_enabled'            => true,
                       ),
                )
            )
        );
    }
}
