<?php
class OwlThemes_Navigation_Model_Category_Attribute_Source_Label extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions() {
        
        if (!$this->_options) {
            $this->_options = array(
                array('value' => 0, 'label' => Mage::helper('owlthemes_navigation')->__('No label')),
                array('value' => 1, 'label' => Mage::helper('owlthemes_navigation')->__('New')),
                array('value' => 2, 'label' => Mage::helper('owlthemes_navigation')->__('Sale')),
                array('value' => 3, 'label' => Mage::helper('owlthemes_navigation')->__('Hot')),
            );
        }
        return $this->_options;
    }
}