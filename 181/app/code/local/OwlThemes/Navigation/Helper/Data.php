<?php
class OwlThemes_Navigation_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getConf($configString) {

        return Mage::getStoreConfig('owlthemes_navigation/' . $configString, Mage::app()->getStore()->getId());
    }

    public function isHomePage() {

        //return true;
        $request = Mage::app()->getFrontController()->getRequest();
        return ($request->getModuleName() == 'cms'
                && $request->getControllerName() == 'index'
                && $request->getActionName() == 'index'); 
    }
    
    public function getNavCss() {
        
        $navVariant = $this->getConf('general/menu_type');
        return 'css' . DS . 'navigation_v' . $navVariant . '.css';
    }

}