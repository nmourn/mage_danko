<?php

class OwlThemes_Navigation_Block_Navigation extends Mage_Catalog_Block_Navigation
{

    protected $renderableAttrCodes;

    protected $outputHelper;
    protected $labelsText;
    protected $labelClasses;
    protected $customContentClasses;
    protected $otNav;
    protected $renderCustomContent;

    const STATIC_BLOCKS_COUNT = 4;

    public function _construct() {

        $this->renderableAttrCodes = array("custom_top_content", "custom_right_content");
        $this->renderCustomContent = true;
        $this->outputHelper = Mage::helper('catalog/output');
        $this->otNav = Mage::helper('owlthemes_navigation');
        $this->labelTexts = array(
            0 => '',
            1 => $this->otNav->__('New'),
            2 => $this->otNav->__('Sale'),
            3 => $this->otNav->__('Hot!'),
        );
        $this->labelClasses = array(
                0 => '',
                1 => 'new',
                2 => 'sale',
                3 => 'hot',
        );
        $this->customContentClasses = array(
            'custom_top_content' => 'navigation_top',
            'custom_right_content' => 'navigation_right',
        );
        parent::_construct();
    }
    
    public function setRenderCustomContent($value) {
        
        $this->renderCustomContent = $value;
    }

    /**
    * Get Custom Navigation Items from admin settings 
    * @return array $items
    */

    protected function getCustomNavItems() {

        $items = array();
        for ($i = 1; $i <= self::STATIC_BLOCKS_COUNT; ++$i) {

            $title = $this->otNav->getConf('custom_items/item' . $i . "_title");
            if ($title) {
                $url = $this->otNav->getConf('custom_items/item' . $i . "_url");
                $hover = $this->otNav->getConf('custom_items/item' . $i . "_hover");
                switch ($hover) {
                    case 1:
                        $identifier = $this->otNav->getConf('custom_items/item' . $i . "_static_block");
                        $hoverContent = $this->getLayout()->createBlock('cms/block')->setBlockId($identifier)->toHtml();
                        break;
                    case 2:
                        $hoverContent = $this->otNav->getConf('custom_items/item' . $i . "_custom_content");
                        break;
                    case 0:
                    default:
                        $hoverContent = "";
                }
                $items[] = array("id" => 1, "title" => $title, "url" => $url, "hover" => $hoverContent);
            }
        }
        return $items;
    }

    /**
    * Get Custom Navigation Items html, ready to use on frontend
    * @return string $html
    */
    public function getCustomNavItemsHtml() {

        $items = $this->getCustomNavItems();
        $html = array();
        if (count($items)) {
            foreach ($items as $item) {
                $url = $item['url'];
                $hover = $item['hover'];
                $html[] = '<li class="level0 static-item' . $item['id'] . '">';
                if ($url) {
                    $html[] = '<a href="' . $url . '">';
                }
                $html[] = '<span>';
                $html[] = $item['title'];
                $html[] = '</span>';
                if ($url) {
                    $html[] = '</a>';
                }
                if ($hover) {
                    $html[] = '<ul>';
                    $html[] = $hover;
                    $html[] = '</ul>';
                }
                $html[] = '</li>';
            }
        }
        return implode("\n", $html);
    }

    /**
    * Render custom attribute to html
    * @param Mage_Core_Catalog_Model_Category $category 
    * @param int $level nesting level number
    * @param string $code attribute code
    * @return string 
    */
    protected function renderCustomAttr($category, $level, $code) { 
        
        //$this->renderableAttrCodes
        $html = "";
        if ($this->renderCustomContent) {
            $value = $this->outputHelper->categoryAttribute($category, $category->getData($code), $code);
            if ($value && $level === 0) {
                $class = $this->customContentClasses[$code];
                $html = '<div class="custom-area ' . $class . '">' . $value . '</div>';
            }
        }
        return $html;
    } 

    /**
    * Get html for custom label 
    * @param Mage_Core_Catalog_Model_Category $category 
    * @param int $level nesting level of category
    * @return string
    */
    protected function getCustomLabel($category, $level) {

        $html = "";
        $label = $category->getData('custom_label');
        $innerModifier = ($level == 0) ? ' ' : '_vn ';
        if ($label && $label > 0 && $label <= 3) {
            $html = '<span class="label_cat' . $innerModifier . $this->labelClasses[$label] . '_label">' . $this->labelTexts[$label] . '</span>';
        }
        return $html;
    }


    /**
     * Render category to html
     *
     * @param Mage_Catalog_Model_Category $category
     * @param int Nesting level number
     * @param boolean Whether ot not this item is last, affects list item class
     * @param boolean Whether ot not this item is first, affects list item class
     * @param boolean Whether ot not this item is outermost, affects list item class
     * @param string Extra class of outermost list items 
     * @param string If specified wraps children list in div with this class (only on first level)
     * @param boolean Whether ot not to add on* attributes to list item
     * @param string opener element html that could be used for accordion-like navigation
     * @return string
     */
    protected function _renderCategoryMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false,
        $isOutermost = false, $outermostItemClass = '', $topChildrenWrapClass = '', $noEventAttributes = false, $openerHtml = '')
    {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();
        $categoryInstance = Mage::getModel('catalog/category')->load($category->getId());

        // get all children
        // If Flat Data enabled then use it but only on frontend
        $flatHelper = Mage::helper('catalog/category_flat');
        if ($flatHelper->isAvailable() && $flatHelper->isBuilt(true) && !Mage::app()->getStore()->isAdmin()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            if ($child->getIsActive()) {
                $activeChildren[] = $child;
            }
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);

        // prepare list item html classes
        $classes = array();
        $classes[] = 'level' . $level;
        $classes[] = 'nav-' . $this->_getItemPosition($level);
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
        }
        $linkClass = '';
        if ($isOutermost && $outermostItemClass) {
            $classes[] = $outermostItemClass;
            $linkClass = ' class="'.$outermostItemClass.'"';
        }
        if ($isFirst) {
            $classes[] = 'first';
        }
        if ($isLast) {
            $classes[] = 'last';
        }
        if ($hasActiveChildren) {
            $classes[] = 'parent';
        }

        if ($customClass = $categoryInstance->getData('css_class')) {
            $classes[] = $customClass;
        }

        // prepare list item attributes
        $attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }
        if ($hasActiveChildren && !$noEventAttributes) {
             $attributes['onmouseover'] = 'toggleMenu(this,1)';
             $attributes['onmouseout'] = 'toggleMenu(this,0)';
        }

        // assemble list item with attributes
        $htmlLi = '<li';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;

        $html[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $html[] = $this->getCustomLabel($categoryInstance, $level);
        $html[] = '</a>';

        // render children
        $htmlChildren = '';
        $j = 0;
        foreach ($activeChildren as $child) {
            $htmlChildren .= $this->_renderCategoryMenuItemHtml(
                $child,
                ($level + 1),
                ($j == $activeChildrenCount - 1),
                ($j == 0),
                false,
                $outermostItemClass,
                "",
                $noEventAttributes,
                $openerHtml
            );
            $j++;
        }
        if (!empty($htmlChildren)) {

            $rightContent = $this->renderCustomAttr($categoryInstance, $level, 'custom_right_content');
            $topContent = $this->renderCustomAttr($categoryInstance, $level, 'custom_top_content');
            
            if ($openerHtml)
                $html[] = $openerHtml;

            $ulClass = "level" . $level;
            if ($rightContent) {
                $ulClass .= " has-right-content";
            }
            $html[] = '<ul class="' . $ulClass . '">';
            $html[] = $topContent;
            if ($topChildrenWrapClass) {
                $html[] = '<div class="' . $topChildrenWrapClass . '">';
            }
            $html[] = $htmlChildren;
            if ($topChildrenWrapClass) {
                $html[] = '</div>';
            }
            
            $html[] = $rightContent;

            $html[] = '</ul>';
            
        }

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;
    }


    /**
     * Render categories menu in HTML
     *
     * @param int Level number for list item class to start from
     * @param string Extra class of outermost list items
     * @param string If specified wraps children list in div with this class (only on first level)
     * @return string
     */
    public function renderCategoriesMenuHtml($level = 0, $outermostItemClass = '', $topChildrenWrapClass = '', $openerHtml = '')
    {
        $activeCategories = array();
        foreach ($this->getStoreCategories() as $child) {
            if ($child->getIsActive()) {
                $activeCategories[] = $child;
            }
        }
        $activeCategoriesCount = count($activeCategories);
        $hasActiveCategoriesCount = ($activeCategoriesCount > 0);

        if (!$hasActiveCategoriesCount) {
            return '';
        }

        $html = '';
        $j = 0;
        foreach ($activeCategories as $category) {
            $html .= $this->_renderCategoryMenuItemHtml(
                $category,
                $level,
                ($j == $activeCategoriesCount - 1),
                ($j == 0),
                true,
                $outermostItemClass,
                $topChildrenWrapClass,
                true,
                $openerHtml
            );
            $j++;
        }

        return $html;
    }
}
