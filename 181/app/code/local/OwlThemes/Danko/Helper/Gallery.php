<?php
class OwlThemes_Danko_Helper_Gallery extends Mage_Core_Helper_Abstract {

    protected $_media;

    /*
    Each thumbnail is array(
        'orig_img' - string path to original image
        'label' - string image label
        'num' - int thumb's number in array
        'resized_img' - string path to resized image (if sizes are specified)
        'retina_img' string path to 2x resized image (if sizes are specified)
    )
    */
    protected $_thumbnails;

    protected $width;
    protected $height;
    protected $productHelper;

    public function __construct() {

        $this->productHelper = Mage::helper('danko/product');
        $this->width = 0;
        $this->height = 0;
        $this->_thumbnails = array();
    }

    public function setMediaBlock($block) {

        $this->_media = $block;
    }

    public function getThumbnails() {

        if (!$this->_thumbnails) 
            $this->_thumbnails = $this->_generateThumbnails();
        return $this->_thumbnails;
    }

    protected function _generateThumbnails() {

        if (!$this->_media) 
            return array();
        $product = $this->_media->getProduct();
        // get main thumb the way magento default media.phtml does
        $mainImg = $this->_media->helper('catalog/image')->init($product, 'image');
        $mainImgTitle = $this->_media->escapeHtml($this->_media->getImageLabel());
        $mainImgSrc = (string) $mainImg;
        $i = 0;
        $thumbs = array();
        $main = array(
            'orig_img' => $mainImgSrc,
            'label' => $mainImgTitle,
            'num' => 0
        );
        // Apparently magento saves the same file in different folders 
        // If we ever meet this file when scanning thumbnails we should avoid to add it
        $mainThumbSrc = str_replace("image", "thumbnail", $mainImgSrc);
        array_push($thumbs, $main);
        foreach ($this->_media->getGalleryImages() as $_image) {
            $thumb = array();
            $i++;
            $title = $this->_media->escapeHtml($_image->getLabel());
            if (strpos($title, "swatch") !== false) continue;
            $img = $this->_media->helper('catalog/image')->init($product, 'thumbnail', $_image->getFile());
            $thumb['orig_img'] = $img->__toString();
            $thumb['label'] = $_image->getLabel();
            $thumb['num'] = $i;
            
            // if ($this->width && $this->height) {
            //     $thumb['resized_img'] = $img->resize($this->width, $this->height)->__toString();
            //     $thumb['retina_img'] = $img->resize($this->width * 2, $this->height * 2)->__toString();
            // }
            if ($thumb['orig_img'] !=  $mainThumbSrc)
                array_push($thumbs, $thumb);
        }
        return $thumbs;
    }

}

