<?php

class OwlThemes_Danko_Helper_Data extends Mage_Core_Helper_Abstract
{

    
    public $mediaPath;
    public $version;

    public function __construct() {

        $this->someVar = "someValue";
        $this->mediaPath = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "owlthemes" . DS . "danko";
        $this->version = Mage::getConfig()->getModuleConfig("OwlThemes_Danko")->version;
    }

    /**
    * Get theme configuration value
    */
    public function getConf($configString, $default = null) {

        return Mage::getStoreConfig('danko_settings/' . $configString, Mage::app()->getStore()->getId());
    }
    
    /**
     * @return string
     */
    public function getCustomCss() {
        
        if ($this->getConf('style/use_custom_css')) {
            return "css" . DS . $this->getConf('style/custom_css_file');
        }
        else return "";
    }
    
    /**
     * @return string
     */
    public function getBgImgUrl() {
        
        $filePath = $this->getConf('style/background_image');
        if (!$filePath || $filePath === "")
            return "";
        return $this->mediaPath . DS . 'backgrounds' . DS . $filePath;
    }
    

    /**
     * @return number
     */
    public function getAspectRatio() {
        
        $ratio = (double) $this->getConf('general/aspect_ratio');
        if (!$ratio)
            $ratio = 1;
        return $ratio;
    }
    
    public function getHomePrecontentBlockId() {
        
        $baseName = "danko-home-before-content";
        $variant = $this->getConf('home_page/homepage_banner_variant');
        return $baseName . "_" . $variant;
    }
    
    public function getLogoSrc($defaultLogoSrc) {
        
        $filePath = $this->getConf('header/logo');
        if (!$filePath || $filePath === "")
            return $defaultLogoSrc;
        return $this->mediaPath . DS . 'logos' . DS . $filePath;
    }

    public function getRetinaLogoSrc($defaultLogoSrc) {

        $filePath = $this->getConf('header/logo_retina');
        if (!$filePath || $filePath === "")
            return $this->getLogoSrc($defaultLogoSrc);
        return $this->mediaPath . DS . 'logos' . DS . $filePath;
    }

    public function getProductBodyClass() {

        return "product-variant-" . $this->getConf('product_page/layout');
    }
}
