<?php
class OwlThemes_Danko_Helper_Product extends Mage_Core_Helper_Abstract {
    
    protected $variant;
    protected $theme;
    protected $imgWidth;
    protected $imgHeight;
    protected $thumbWidth;
    protected $thumbHeight;
    protected $thumbCount;
    protected $imgContainerClass;
    protected $descContainerClass;
    protected $wrapperClass;
    protected $zoomParams;
    
    public function __construct() {
        
        $pageVariants = array(
            'default' => array(
                'thumb_count' => 3,
                'image_container_class' => 'col-xs-12 col-sm-4 col-md-4 col-lg-4',
                'descr_container_class' => 'col-xs-12 col-sm-6 col-md-6 col-lg-6',
                'wrapper_class' => 'product_page_one',
                'zoom_params' => array(
                    array(
                        'screenWidth' => 800, 
                        'thumbWidth' => 240,  
                        'areaWidth' => 350, 
                    ),
                    array(
                        'screenWidth' => 1000, 
                        'thumbWidth' => 240, 
                        'thumbHeight' => 280, 
                        'areaWidth' => 350, 
                    ),
                    array(
                        'screenWidth' => 1050, 
                        'thumbWidth' => 313,  
                        'areaWidth' => 460, 
                    ),
                    array(
                        'screenWidth' => 1200, 
                        'thumbWidth' => 313,  
                        'areaWidth' => 460, 
                    ),
                    array(
                        'screenWidth' => 1250, 
                        'thumbWidth' => 380,  
                        'areaWidth' => 560, 
                    ),
                )
            ),
            'wide' =>  array(
                'thumb_count' => 3,
                'image_container_class' => 'col-xs-12 col-sm-4 col-md-4 col-lg-4',
                'descr_container_class' => 'col-xs-12 col-sm-8 col-md-8 col-lg-8',
                'wrapper_class' => 'row product_page_one',
                'zoom_params' => array(
                    array(
                        'screenWidth' => 800, 
                        'thumbWidth' => 240,  
                        'areaWidth' => 450, 
                    ),
                    array(
                        'screenWidth' => 1000, 
                        'thumbWidth' => 240,  
                        'areaWidth' => 450, 
                    ),
                    array(
                        'screenWidth' => 1050, 
                        'thumbWidth' => 313,  
                        'areaWidth' => 600, 
                    ),
                    array(
                        'screenWidth' => 1200, 
                        'thumbWidth' => 313,  
                        'areaWidth' => 600, 
                    ),
                    array(
                        'screenWidth' => 1250, 
                        'thumbWidth' => 380,  
                        'areaWidth' => 730, 
                    ),
                )
            ),
            'widelarge' =>  array(
                'thumb_count' => 4,
                'image_container_class' => 'col-xs-12 col-sm-5 col-md-5 col-lg-5',
                'descr_container_class' => 'col-xs-12 col-sm-7 col-md-7 col-lg-7',
                'wrapper_class' => 'row product_page_one',
                'zoom_params' => array(
                    array(
                        'screenWidth' => 800, 
                        'thumbWidth' => 300,  
                        'areaWidth' => 400, 
                    ),
                    array(
                        'screenWidth' => 1000, 
                        'thumbWidth' => 300,  
                        'areaWidth' => 400, 
                    ),
                    array(
                        'screenWidth' => 1050, 
                        'thumbWidth' => 390,  
                        'areaWidth' => 520, 
                    ),
                    array(
                        'screenWidth' => 1200, 
                        'thumbWidth' => 390,  
                        'areaWidth' => 520, 
                    ),
                    array(
                        'screenWidth' => 1250, 
                        'thumbWidth' => 475,  
                        'areaWidth' => 635, 
                    ),
                )
            ),
            'vertical' =>  array(
                'thumb_count' => 3,
                'image_container_class' => 'col-xs-12 col-sm-5 col-md-5 col-lg-5',
                'descr_container_class' => 'col-xs-12 col-sm-5 col-md-5 col-lg-5',
                'wrapper_class' => 'product_page_four',
                'zoom_params' => array(
                    array(
                        'screenWidth' => 800, 
                        'thumbWidth' => 220,  
                        'areaWidth' => 292, 
                    ),
                    array(
                        'screenWidth' => 1000, 
                        'thumbWidth' => 220,  
                        'areaWidth' => 292, 
                    ),
                    array(
                        'screenWidth' => 1050, 
                        'thumbWidth' => 290,  
                        'areaWidth' => 380, 
                    ),
                    array(
                        'screenWidth' => 1200, 
                        'thumbWidth' => 290,  
                        'areaWidth' => 380, 
                    ),
                    array(
                        'screenWidth' => 1250, 
                        'thumbWidth' => 350,  
                        'areaWidth' => 470, 
                    ),
                )
            ),
        );
        
        $this->theme = Mage::helper('danko');
        $aspectRatio = $this->theme->getAspectRatio();
        $this->variant = $this->theme->getConf('product_page/layout');
        $this->thumbCount = $pageVariants[$this->variant]['thumb_count'];
        $this->imgContainerClass = $pageVariants[$this->variant]['image_container_class'];
        $this->descContainerClass = $pageVariants[$this->variant]['descr_container_class'];
        $this->wrapperClass = $pageVariants[$this->variant]['wrapper_class'];
        $this->zoomParams = $pageVariants[$this->variant]['zoom_params'];
        foreach ($this->zoomParams as $key => $param) {

            $this->zoomParams[$key]['srcWidth'] = 900;
            $this->zoomParams[$key]['thumbHeight'] = (int) ($param['thumbWidth'] * $aspectRatio);
            $this->zoomParams[$key]['srcHeight'] = (int) (900 * $aspectRatio);
        }
    }
    
    public function getPageVariant() {
        
        return $this->variant;
    }
    
    public function getThumbCount() {
        return $this->thumbCount;
    }
    
    public function getImgContainerClass() {
        return $this->imgContainerClass;
    }
    
    public function getDescContainerClass() {
        return $this->descContainerClass;
    }
    
    public function getWrapperClass() {
        return $this->wrapperClass;
    }

    public function getZoomParams() {

        return Mage::helper('core')->jsonEncode($this->zoomParams);
    }

    public function getSizeguideSrc() {

        $filePath = $this->theme->getConf('product_page/size_guide');
        if ($filePath) {
            return $this->theme->mediaPath . DS . 'sizeguide' . DS . $filePath;
        }
        return "";
    }
}
