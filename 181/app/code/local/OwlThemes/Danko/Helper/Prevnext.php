<?php
class OwlThemes_Danko_Helper_Prevnext extends Mage_Core_Helper_Abstract {
    
    protected $ids;
    protected $productPos;
    
    
    public function __construct() {
        
        $prodId = Mage::registry('current_product')->getId();
        $currentCat = Mage::registry('current_category');
        if (!$currentCat) {
            $productCats = Mage::registry('current_product')->getCategoryIds();
            if (count($productCats) > 0) {
                $currentCat = Mage::getModel('catalog/category')->load($productCats[0]);
            }
        }
        if ($currentCat) {
            $collection = $currentCat->getProductCollection()->addAttributeToSort('position', 'asc');
            $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
            $this->ids = $collection->getAllIds();
            $this->productPos = array_search($prodId, $this->ids);
        }
    }
    
    protected function getProductUrl($productId) {
        
        $product = Mage::getModel('catalog/product')->load($productId);
        $urlpath = $product->getUrlPath();
        if (!$urlpath) {
            $url = $product->getProductUrl();
        }
        else {
            $url = Mage::getUrl($urlpath);
        }
        return $url;
    }
    
    public function getPreviousProduct() {
        
        $prevPos = $this->productPos - 1;
        if (isset($this->ids[$prevPos])) {
            $prevId = $this->ids[$prevPos];
        }
        else {
            $prevId = end($this->ids);
        }
        
        return $this->getProductUrl($prevId);
    }
    
    public function getNextProduct() {
        
        $nextPos = $this->productPos + 1;
        if (isset($this->ids[$nextPos])) {
            $nextId = $this->ids[$nextPos];
        }
        else {
            $nextId = reset($this->ids);
        }
        return $this->getProductUrl($nextId);
    }
}