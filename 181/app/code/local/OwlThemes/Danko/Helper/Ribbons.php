<?php
class OwlThemes_Danko_Helper_Ribbons extends Mage_Core_Helper_Abstract {


    protected $product;
    protected $theme;

    public function __construct() {

        $this->theme = Mage::helper('danko');
    }
    /**
    * Get all ribbons html for given product
    * @param Mage_Catalog_Model_Product $product
    * @return string
    */
    public function getRibbonsHtml($product) {

        $this->product = $product;
        $html = "";
        if ($this->isNew()) {
            $html .= '<div class="ribbon-new">' . $this->__('New') . '</div>';
        }
        if ($this->isOnSale()) {
            $percentage = "";
            if ($this->theme->getConf('category_page/sale_ribbon_percentage')) {
                $percentage = sprintf("%d%%", $this->getSalePercentage());
                $saleText = $percentage . " " . $this->__('Off');
            }
            else {
                $saleText = $this->__('Sale');
            }
            $saleClasses = "ribbon-sale";
            if (!$this->isNew()) $saleClasses .= " top";

            $html .= sprintf('<div class="%s">%s</div>', $saleClasses, $saleText);
        }

        return $html;
    }

    protected function isNew() {

        return $this->checkDate('news_from_date', 'news_to_date');
    }

    protected function isOnSale() {

        $specialPrice = $this->product->getSpecialPrice();
        $price = $this->product->getPrice();
        if ($specialPrice && $specialPrice < $price) {
            return $this->checkDate('special_from_date', 'special_to_date');
        }
    }

    protected function getSalePercentage() {

        $special = $this->product->getSpecialPrice();
        $original = $this->product->getPrice();
        $percentage = $special / floatval($original);
        return 100 - intval($percentage * 100);

    }

    /**
    * Check if current date fits between two given attributes (both are datetime in product)
    * @param $fromAttr string from attribute 
    * @param $toAttr string from attribute 
    * @return boolean
    */
    protected function checkDate($fromAttr, $toAttr) {

        $from = $this->product->getData($fromAttr);
        $to = $this->product->getData($toAttr);
        if (!$from && !$to) {
            return false;
        }

        $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
        if (isset($from)) {
            $from = strtotime($from);
            $bottomRange = ($currentTimestamp > $from);
        }
        else {
            $bottomRange = true;
        }
        if (isset($to)) {
            $to = strtotime($to);
            $topRange = ($currentTimestamp < $to);
        }
        else {
            $topRange = true;
        }
        return ($bottomRange && $topRange);
    }
}