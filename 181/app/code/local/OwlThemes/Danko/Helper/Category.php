<?php
class OwlThemes_Danko_Helper_Category extends Mage_Core_Helper_Abstract {
    
    protected $theme;
    protected $template;
    protected $countPerRow;
    protected $imgWidth;
    protected $imgHeight;
    protected $containerGridClass;
    protected $imgHelper;
    
    public function __construct() {
        
        $this->theme = Mage::helper('danko');
        $this->countPerRow = $this->theme->getConf('category_page/per_row');
        $this->template = $this->theme->getConf('category_page/default_template');
        $this->imgHelper = Mage::helper('catalog/image');
        $this->calculateData();
    }

    public function calculateData() {

        $containerGridClasses = array(
            '2' => 'col-xs-12 col-sm-6 col-md-6 col-lg-6',
            '3' => 'col-xs-12 col-sm-4 col-md-4 col-lg-4',
            '4' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3',
            '6' => 'col-xs-12 col-sm-6 col-md-4 col-lg-2'
        );
        $gridWidths = array(
            // sizes for 1 column template
            1 => array('2' => 410,
                       '3' => 410,
                       '4' => 410, 
                       '6' => 410), 
            //sizes for 2 column template
            2 => array('2' => 410,
                       '3' => 410, 
                       '4' => 410,
                       '6' => 410));
        $listWidths = array(
                1 => 650,
                2 => 650);
        $dlistWidths = array(
                1 => 410,
                2 => 410);
        $aspectRatio = $this->theme->getAspectRatio();
        $templateNum = ($this->template == 'page/2columns-left.phtml') ? 2 : 1;
        $this->countPerRow = array(
            'list' => 1,
            'double' => 2,
            'grid' => $this->countPerRow
        );

        $this->imgWidth = array();
        $this->imgWidth['grid'] = $gridWidths[$templateNum][$this->countPerRow['grid']];
        $this->imgWidth['list'] = $listWidths[$templateNum];
        $this->imgWidth['double'] = $dlistWidths[$templateNum];
        
        $this->imgHeight = array();
        foreach ($this->imgWidth as $k => $v) {
            $this->imgHeight[$k] = $v * $aspectRatio;
        }
        
        $this->containerGridClass = $containerGridClasses[$this->countPerRow['grid']];
        return $this;
    }

    public function setColCount($colCount) {

        $this->countPerRow = $colCount;
        return $this;
    }

    public function setTemplate($template) {

        $this->template = $template;
        return $this;
    }
    
    public function getTemplate() {
        
        return $this->template;
    }
    
    public function getImgWidth($viewType) {
    
        return $this->imgWidth[$viewType];
    }
    
    public function getImgHeight($viewType) {
        
        return $this->imgHeight[$viewType];
    }
    
    public function getImgRetinaWidth($viewType) {
        
        return $this->getImgWidth($viewType) * 2;
    }

    public function getImgRetinaHeight($viewType) {
    
        return $this->getImgHeight($viewType) * 2;
    }
    
    public function getContainerGridClass() {
        
        return $this->containerGridClass;
    }
    
    public function getCountPerRow($viewType) {
        
        return $this->countPerRow[$viewType];
    }
    
}