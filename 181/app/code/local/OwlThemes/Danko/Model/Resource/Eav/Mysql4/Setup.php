<?php

class OwlThemes_Danko_Model_Resource_Eav_Mysql4_Setup extends Mage_Catalog_Model_Resource_Setup {

    public function getDefaultEntities() {

        return array(
            'catalog_product'                => array(
                'entity_model'                   => 'catalog/product',
                'attribute_model'                => 'catalog/resource_eav_attribute',
                'table'                          => 'catalog/product',
                'additional_attribute_table'     => 'catalog/eav_attribute',
                'entity_attribute_collection'    => 'catalog/product_attribute_collection',
                'attributes'                     => array(
                    'owlthemes_hoverswap_image'              => array(
                        'type'                       => 'varchar',
                        'label'                      => 'Swap Image',
                        'input'                      => 'media_image',
                        'frontend'                   => 'catalog/product_attribute_frontend_image',
                        'required'                   => false,
                        'sort_order'                 => 150,
                        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'group'                      => 'Images',
                    ),
                    'owlthemes_featured'            => array(
                        'type'                       => 'text',
                        'label'                      => 'Is Featured',
                        'input'                      => 'select',
                           'source'                     => 'eav/entity_attribute_source_boolean',
                           'required'                   => false,
                           'sort_order'                 => 200,
                           'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                        'used_in_product_listing'    => true,
                    ),
                    'owlthemes_custom_tab_title'      => array(
                        'type'                       => 'text',
                           'label'                      => 'Custom tab title',
                           'input'                      => 'text',
                           'required'                   => false,
                          'sort_order'                 => 204,
                         'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                    ),
                    'owlthemes_custom_tab'        => array(
                        'type'                       => 'text',
                           'label'                      => 'Custom tab',
                           'input'                      => 'textarea',
                           'required'                   => false,
                          'sort_order'                 => 205,
                           'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                         'wysiwyg_enabled'            => true,
                           'is_html_allowed_on_front'   => true,
                       ),
                )
                    
            )
        );
    }
}