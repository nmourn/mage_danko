<?php

class OwlThemes_Danko_Model_Design_Package extends Mage_Core_Model_Design_Package {

    protected function _fallback($file, array &$params, array $fallbackScheme = array(array())) {   

        // if additional fallback to default/default is disabled
        if (!Mage::helper("danko")->getConf("other/force_default_fallback")) {
            return parent::_fallback($file, $params, $fallbackScheme);
        }
        if ($this->_shouldFallback) {
            foreach ($fallbackScheme as $try) {
                $params = array_merge($params, $try);
                $filename = $this->validateFile($file, $params);
                if ($filename) {
                    return $filename;
                }
            }

            // try to load file from default/default
            $params['_package'] = self::DEFAULT_PACKAGE;
            $params['_theme']   = self::DEFAULT_THEME;
            $filename = $this->validateFile($file, $params);
            if ($filename) {
                return $filename;
            }

            // otherwise take file from base/default
            $params['_package'] = self::BASE_PACKAGE;
            $params['_theme']   = self::DEFAULT_THEME;
        }
        return $this->_renderFilename($file, $params);
    }
}