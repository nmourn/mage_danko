<?php

class OwlThemes_Danko_Model_System_Config_Source_Product_Layout {

    public function toOptionArray() {

        return array(
            array('value' => 'default', 'label' => Mage::helper('danko')->__('Default')),
            array('value' => 'wide', 'label' => Mage::helper('danko')->__('Wide')),
            array('value' => 'widelarge', 'label' => Mage::helper('danko')->__('Wide (large image)')),
            array('value' => 'vertical', 'label' => Mage::helper('danko')->__('Vertical')),
        );
    }
}