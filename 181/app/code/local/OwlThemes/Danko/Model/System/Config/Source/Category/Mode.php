<?php

class OwlThemes_Danko_Model_System_Config_Source_Category_Mode {

    public function toOptionArray() {

        return array(
            array('value' => 'grid', 'label' => Mage::helper('danko')->__('Grid')),
            array('value' => 'list', 'label' => Mage::helper('danko')->__('List')),
            array('value' => 'double', 'label' => Mage::helper('danko')->__('Double List'))
        );
    }
}