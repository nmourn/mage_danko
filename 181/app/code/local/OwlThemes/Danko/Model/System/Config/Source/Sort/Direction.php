<?php
class OwlThemes_Danko_Model_System_Config_Source_Sort_Direction {
    
    public function toOptionArray() {
    
        return array(
            array('value' => 'asc', 'label' => Mage::helper('danko')->__('Ascending')),
            array('value' => 'desc', 'label' => Mage::helper('danko')->__('Descending')),
        );
    }
}