<?php

class OwlThemes_Danko_Model_System_Config_Source_Style_Background_Repeat {

    public function toOptionArray() {

        return array(
            array('value' => 'no-repeat', 'label' => Mage::helper('danko')->__('no-repeat')),
            array('value' => 'repeat', 'label' => Mage::helper('danko')->__('repeat')),
            array('value' => 'repeat-x', 'label' => Mage::helper('danko')->__('repeat-x')),
            array('value' => 'repeat-y', 'label' => Mage::helper('danko')->__('repeat-y')),
            array('value' => 'initial', 'label' => Mage::helper('danko')->__('initial')),
            array('value' => 'inherit', 'label' => Mage::helper('danko')->__('inherit')),
        );
    }
}