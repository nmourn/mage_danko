<?php

class OwlThemes_Danko_Model_System_Config_Source_Style_Background_Attachment {

    public function toOptionArray() {

        return array(
            array('value' => 'initial', 'label' => Mage::helper('danko')->__('initial')),
            array('value' => 'fixed', 'label' => Mage::helper('danko')->__('fixed')),
            array('value' => 'local', 'label' => Mage::helper('danko')->__('local')),
            array('value' => 'scroll', 'label' => Mage::helper('danko')->__('scroll')),
            array('value' => 'inherit', 'label' => Mage::helper('danko')->__('inherit')),
        );
    }
}