<?php
class OwlThemes_Danko_Model_System_Config_Source_Banner_Hover {

    public function toOptionArray() {

        return array(

            array("value" => 'good', "label" => Mage::helper('danko')->__('Good')),
            array("value" => 'bad', "label" => Mage::helper('danko')->__('Bad')),
        );
    }
}