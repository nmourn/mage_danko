<?php
class OwlThemes_Danko_Model_System_Config_Source_Footer_Type {

    public function toOptionArray() {

        return array(
            array('value' => '1', 'label' => Mage::helper('danko')->__('Type 1')),
            array('value' => '2', 'label' => Mage::helper('danko')->__('Type 2')),
        );
    }
}