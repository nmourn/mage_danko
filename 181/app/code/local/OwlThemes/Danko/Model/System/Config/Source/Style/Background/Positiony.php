<?php

class OwlThemes_Danko_Model_System_Config_Source_Style_Background_Positiony {

    public function toOptionArray() {

        return array(
            array('value' => 'top',    'label' => Mage::helper('danko')->__('top')),
            array('value' => 'center',    'label' => Mage::helper('danko')->__('center')),
            array('value' => 'bottom',    'label' => Mage::helper('danko')->__('bottom'))
        );
    }
}