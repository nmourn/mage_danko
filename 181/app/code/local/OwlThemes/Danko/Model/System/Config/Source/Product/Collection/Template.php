<?php
class OwlThemes_Danko_Model_System_Config_Source_Product_Collection_Template {
    
    public function toOptionArray() {
    
        return array(
            array('value' => 'catalog/product/list/slider.phtml', 'label' => Mage::helper('danko')->__('Slider')),
            array('value' => 'catalog/product/list/grid.phtml', 'label' => Mage::helper('danko')->__('Grid')),
        );
    }
}