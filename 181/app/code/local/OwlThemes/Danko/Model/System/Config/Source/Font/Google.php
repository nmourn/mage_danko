<?php
class OwlThemes_Danko_Model_System_Config_Source_Font_Google {

     private $fontsList = "Open Sans,Roboto,Oswald,Lato,Droid Sans,Open Sans Condensed,PT Sans,Source Sans Pro,Droid Serif,Roboto Condensed,Ubuntu,PT Sans Narrow,Montserrat,Raleway,Lora,Yanone Kaffeesatz,Francois One,Arimo,Bitter,Arvo,Lobster,Oxygen,Merriweather,Varela Round,Titillium Web,Noto Sans,Dosis,PT Serif,Cabin,Roboto Slab,Play,Inconsolata,Rokkitt,Ubuntu Condensed,Libre Baskerville,Abel,Josefin Sans,Muli,Fjalla One,Cuprum,Playfair Display,Hammersmith One,Indie Flower,Maven Pro,Audiowide,Armata,Nunito,Signika,Anton,News Cycle,Archivo Narrow,Shadows Into Light,Merriweather Sans,Asap,Vollkorn,Questrial,Cardo,Pacifico,Bree Serif,Dancing Script,The Girl Next Door,PT Sans Caption,Nobile,Coming Soon,Quicksand,Istok Web,Changa One,Droid Sans Mono,Karla,Poiret One,Alegreya,Allan,Exo,Cabin Condensed,Kreon,Philosopher,Chewy,Pontano Sans,Monda,Voltaire,Gudea,Ubuntu Mono,Noto Serif,Patua One,Varela,Pathway Gothic One,Squada One,Playball,Ropa Sans,Righteous,Quattrocento Sans,Crimson Text,Cantarell,Crafty Girls,Nothing You Could Do,Gloria Hallelujah,Montserrat Alternates,Rock Salt,Josefin Slab,Luckiest Guy,Crete Round,Special Elite,Black Ops One,Archivo Black,Cantata One,Gentium Book Basic,Telex,Fredoka One,Bevan,Lobster Two,Permanent Marker,Economica,Lemon,Bangers,EB Garamond,Satisfy,Noticia Text,Old Standard TT,Covered By Your Grace,Comfortaa,Calligraffitti,Tinos,Amatic SC,Orbitron,Shadows Into Light Two,Amaranth,Architects Daughter,Jockey One,BenchNine,Lusitana,Marvel,Source Code Pro,Cherry Cream Soda,Handlee,Patrick Hand,Allerta,Chivo,Sanchez,Molengo,Passion One,Oleo Script,Paytone One,Reenie Beanie,Tangerine,Quattrocento,Didact Gothic";
 
    public function toOptionArray()
    {
        $fonts = explode(',', $this->fontsList);
        $options = array();
        foreach ($fonts as $font) {
            $options[] = array(
                'value' => $font,
                'label' => $font,
            );
        }
 
        return $options;
    }
}