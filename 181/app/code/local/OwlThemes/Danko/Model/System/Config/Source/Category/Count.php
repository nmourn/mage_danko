<?php

class OwlThemes_Danko_Model_System_Config_Source_Category_Count {

    public function toOptionArray() {

        return array(
            array('value' => '2', 'label' => Mage::helper('danko')->__('2')),
            array('value' => '3', 'label' => Mage::helper('danko')->__('3')),
            array('value' => '4', 'label' => Mage::helper('danko')->__('4')),
            array('value' => '6', 'label' => Mage::helper('danko')->__('6')),
        );
    }
}