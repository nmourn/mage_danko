<?php

class OwlThemes_Danko_Model_System_Config_Source_Style_Background_Positionx {

    public function toOptionArray() {

        return array(
            array('value' => 'left',    'label' => Mage::helper('danko')->__('left')),
            array('value' => 'center',    'label' => Mage::helper('danko')->__('center')),
            array('value' => 'right',    'label' => Mage::helper('danko')->__('right'))
        );
    }
}