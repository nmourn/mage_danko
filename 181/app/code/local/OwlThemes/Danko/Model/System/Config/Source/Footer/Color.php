<?php
class OwlThemes_Danko_Model_System_Config_Source_Footer_Color {

    public function toOptionArray() {

        return array(
            array('value' => 'light', 'label' => Mage::helper('danko')->__('Light')),
            array('value' => 'dark', 'label' => Mage::helper('danko')->__('Dark')),
        );
    }
}