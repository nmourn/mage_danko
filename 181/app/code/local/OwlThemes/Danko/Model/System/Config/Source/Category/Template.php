<?php

class OwlThemes_Danko_Model_System_Config_Source_Category_Template {

    public function toOptionArray() {

        return array(
            array('value' => 'page/1column.phtml', 'label' => Mage::helper('danko')->__('One column')),
            array('value' => 'page/2columns-left.phtml', 'label' => Mage::helper('danko')->__('Two columns'))
        );
    }
}