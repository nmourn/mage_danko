<?php
class OwlThemes_Danko_Model_System_Config_Source_Sort_Attribute {
    
    public function toOptionArray() {
    
        return array(
            array('value' => 'entity_id', 'label' => Mage::helper('danko')->__('Id')),
            array('value' => 'name', 'label' => Mage::helper('danko')->__('Name')),
            array('value' => 'ordered_qty', 'label' => Mage::helper('danko')->__('Sales')),
            array('value' => 'price', 'label' => Mage::helper('danko')->__('Price')),
            array('value' => 'created_at', 'label' => Mage::helper('danko')->__('Date created')),
            array('value' => 'sku', 'label' => Mage::helper('danko')->__('SKU')),
        );
    }
}