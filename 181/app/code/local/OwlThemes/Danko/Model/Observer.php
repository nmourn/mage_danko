<?php
 
class OwlThemes_Danko_Model_Observer {

    public function cmsPageRenderBefore(Varien_Event_Observer $observer) {

        $layoutUpdate = Mage::app()->getLayout()->getUpdate();
        $handles = $layoutUpdate->getHandles();

        if (in_array("cms_index_index", $handles) && in_array("page_three_columns", $handles)) {
            $layoutUpdate->addHandle("homepage_three_columns");
        }
    }
}