<?php
class OwlThemes_Danko_Block_Widget_Product_Collection extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {
    
    protected $collectionBlock;
    
    protected function _construct() {
        
        parent::_construct();
        $params = $this->getWidgetSettings();
        $this->collectionBlock = Mage::app()->getLayout()->createBlock('danko/product_collection', '', $params);
    }
    
    protected function getWidgetSettings() {
        
        $params = array(
                'template' => $this->_getData('template'),
                'col_count' => $this->_getData('col_count'),
                'categories' => $this->_getData('categories'),
                'new' => $this->_getData('new'),
                'sale' => $this->_getData('sale'),
                'featured' => $this->_getData('featured'),
                'order_by' => $this->_getData('order_by'),
                'order_dir' => $this->_getData('order_dir'),
                'count' => $this->_getData('count'),
                'random' => $this->_getData('random'),
                'title' => $this->_getData('title'),
                'hide_addtocart' => $this->_getData('hide_addtocart'),
                'hide_price' => $this->_getData('hide_price'),
                'hide_hoveritems' => $this->_getData('hide_hoveritems'),
                'hide_stars' => $this->_getData('hide_stars'),
                'hide_ribbons' => $this->_getData('hide_ribbons'),
        );
        return $params;
    }
    
    protected function _toHtml() {
        
         return $this->collectionBlock->_toHtml();
    }
}