<?php
class OwlThemes_Danko_Block_Widget_Banner extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {


    private function getBannerText($identifier) {

        $value = $this->_getData('banner_' . $identifier . '_text');
        return '<div class="banner-html'.'">'.$value.'</div>';
    }

    private function getBannerImage($identifier) {

        $value = $this->_getData('banner_' . $identifier . '_image');
        if ($value) 
            return '<div class="banner-img"><img src="' . $value . '" /></div>';
        else
            return "";
    }

    private function getBannerClass($identifier) {

        $hoverEffect = $this->_getData('banner_' . $identifier . '_effect');
        return "emt-banner banner-" . $identifier . " hover-" . $hoverEffect;
    }

    /**
    * Produce html for a single banner by giving identifier
    * @param $identifier int|string could be banner number like 11 or 23 or "custom" 
    * @return string
    */
    private function getBannerHtml($identifier, $width=0, $height=0) {

        $html = array();
        if ($width != 0 && $height != 0) {
            $style = 'style="width' . $width . 'px;';
            $style .= 'height: ' . $height . 'px;"';
        }
        else {
            $style = "";
        }
        $html[] = '<div class="'. $this->getBannerClass($identifier) .'"' . $style . '>';
        if ($identifier != "custom") {
            $html[] =  $this->getBannerText($identifier);
            $html[] =  $this->getBannerImage($identifier);
        }
        else {
            $html[] = 'CUSTOM WILL BE SOON';
        }
        $html[] = '</div>';
        return implode("\n", $html);
    }

    protected function _toHtml() {

        $html = array();
        $type = $this->_getData('banner_type');
        
        $html[] = '<div class="emt-banners-wrapper">';
        switch ($type) {

            case "single":
                $html[] = $this->getBannerHtml(11);
                break;
            case "two":
                $html[] = $this->getBannerHtml(12);
                $html[] = $this->getBannerHtml(22);
                break;
            case "three":
                $html[] = $this->getBannerHtml(13);
                $html[] = $this->getBannerHtml(23);
                $html[] = $this->getBannerHtml(33);
                break;
            case "custom":
            default:
                $html[] = $this->getBannerHtml("custom", $this->_getData('banner_custom_width'), $this->_getData('banner_custom_height'));
                break;
        }
        $html[] = '</div>';
        // $idPath = explode("/", $this->_getData('id_path'));
        // if (isset($idPath[1])) {

        //     $id = $idPath[1];
        //     if ($id) {
        //         $product = Mage::getModel('catalog/product')->load($id);
        //         $html = $product->getName();
        //     }
        // }
        
        return implode("\n", $html);
    }
}

