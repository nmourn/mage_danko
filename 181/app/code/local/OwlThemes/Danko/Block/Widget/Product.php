<?php
class OwlThemes_Danko_Block_Widget_Product extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {

    protected function _toHtml() {

        $html = "";
        $idPath = explode("/", $this->_getData('id_path'));
        if (isset($idPath[1])) {

            $id = $idPath[1];
            if ($id) {
                $product = Mage::getModel('catalog/product')->load($id);
                $html = $product->getName();
            }
        }

        return "<div>" . $html . "</div>";
    }
}

