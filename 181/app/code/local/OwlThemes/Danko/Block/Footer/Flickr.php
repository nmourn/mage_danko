<?php
class OwlThemes_Danko_Block_Footer_Flickr extends Mage_Core_Block_Template {

    protected $username;
    protected $apiKey;
    protected $count;
    protected $theme;
    protected $cache;

    const CACHE_KEY = 'owlthemes_flickr';
    const CACHE_LIFETIME = 300;

    public function _construct() {

        $this->theme = Mage::helper('danko');
        $this->username = $this->theme->getConf('footer/flickr_username');
        $this->apiKey = $this->theme->getConf('footer/flickr_api_key');
        $this->count = 8;
        $this->cache = Mage::app()->getCache();
        //$this->setData('cache_lifetime', 0);
    }

    /**
    * Get images from cache by key
    */
    protected function loadCache($key=self::CACHE_KEY) {

        $value = $this->cache->load($key);
        return simplexml_load_string($value);
    }

    /**
    * save images to cache providing key and lifetime
    * @param SimpleXMLElement $images from flickr screen to cache
    */
    protected function saveCache($images, $key=self::CACHE_KEY, $lifetime=self::CACHE_LIFETIME) {

        $value = $images->asXML();
        $this->cache->save($value, $key, array('owlthemes_flickr'), $lifetime);
    }

    public function getUserId() {

        $url = 'https://api.flickr.com/services/rest/?api_key=' . urlencode($this->apiKey) . '&method=flickr.urls.lookupUser&url=http://www.flickr.com/photos/' . urlencode($this->username) . '/';
        $return = $this->fetch($url);
        return $return->user->attributes()->id;
    }
 
    public function getImages() {

        if (!$images = $this->loadCache()) {
            //echo "CACHE MISS";
            $url = 'https://api.flickr.com/services/rest/?api_key=' . urlencode($this->apiKey) . '&method=flickr.photos.search&user_id=' . $this->getUserId() . '&per_page=' . urlencode($this->count);
            $images = $this->fetch($url);
            $this->saveCache($images);
        }
        return $images;
    }

    protected function fetch($url, $post = false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Flickr');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        if((int)$response['http_code'] == 200) {
            return new SimpleXMLElement($output);
        }
        else {
            return null;
        }
    }


}