<?php
class OwlThemes_Danko_Block_Footer_Twitter extends Mage_Core_Block_Template {

    protected $username;
    protected $tweetCount;
    protected $config;

    protected $tweets;
    protected $errorText;
    protected $cache;
    protected $theme;
    const CACHE_KEY = "owlthemes_tweets";
    const CACHE_BACKUP_KEY = "owlthemes_tweets_backup";
    const CACHE_TAG = "owlthemes_twitter";
    const CACHE_LIFETIME = 300; // in seconds
    const CACHE_BACKUP_LIFETIME = 2592000; 


    public function _construct() {

        $this->theme = Mage::helper('danko');
        $this->config = array(
            'consumer_key' => $this->theme->getConf('footer/twitter_consumer_key'),
            'consumer_secret' => $this->theme->getConf('footer/twitter_consumer_secret'),
            'oauth_token' => $this->theme->getConf('footer/twitter_oauth_token'),
            'oauth_token_secret' => $this->theme->getConf('footer/twitter_oauth_token_secret'),
            'output_format' => 'object'
        );
        $this->username = $this->theme->getConf('footer/twitter_username');
        $this->tweetCount = $this->theme->getConf('footer/twitter_count');
        $this->cache = Mage::app()->getCache();
        $this->update();
        parent::_construct();
    }

    protected function update() {

        // if cache is invalidated, try to update it
        if (!$tweets = $this->loadCache(self::CACHE_KEY)) {
            try {
                $newTweets = $this->loadNewTweets();
                $this->tweets = $newTweets;
                //save to cache and to backup cache
                $this->saveCache($newTweets, self::CACHE_KEY, self::CACHE_LIFETIME);
                $this->saveCache($newTweets, self::CACHE_BACKUP_KEY, self::CACHE_BACKUP_LIFETIME);
            }
            catch (Exception $e) {
                if (!$this->tweets = $this->loadCache(self::CACHE_BACKUP_KEY)) {
                    $this->errorText = $e->getMessage();
                }
            }
        }
        else {
            $this->tweets = $tweets;
        }
    }

    /**
    * Get tweets from cache by key
    */
    protected function loadCache($key) {

        $value = $this->cache->load($key);
        return unserialize($value);
    }

    /**
    * save tweets to cache providing key and lifetime
    */
    protected function saveCache($data, $key, $lifetime) {

        $value = serialize($data);
        $this->cache->save($value, $key, array(self::CACHE_TAG), $lifetime);
    }

    /**
    * Request tweets feed from twitter
    * @return array of strings $tweets
    * @todo
    */
    protected function loadNewTweets() {

        $tw = new OwlThemes_TwitterOAuth($this->config);
        $params = array(
         'screen_name' => $this->username,
         'count' => $this->tweetCount,
         'exclude_replies' => true
        );
        $response = $tw->get('statuses/user_timeline', $params);
        return $response;
    }

    /**
    * extract data to be displayed from raw tweets array
    */
    protected function extractData() {

        $formatted = array();
        foreach ($this->tweets as $tweet) {
            $text = $tweet->text;
            $text = preg_replace('/(https?:\/\/\S+)/', '<a href="\1">\1</a>', $text);
            $text = preg_replace('/(^|\s)@(\w+)/', '\1@<a href="http://twitter.com/\2">\2</a>', $text);
            $text = preg_replace('/(^|\s)#(\w+)/', '\1#<a href="http://search.twitter.com/search?q=%23\2">\2</a>', $text);
            $formatted[] = $text;
        }
        return $formatted;
    }


    public function getTweets() {

        if ($this->errorText && !$this->tweets) {
            return $this->errorText;
        }
        else if ($this->tweets) {
            return $this->extractData();
        }
        else {
            return "Unknown error";
        }
    }
}