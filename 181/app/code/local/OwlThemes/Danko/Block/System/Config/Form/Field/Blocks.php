<?php
class OwlThemes_Danko_Block_System_Config_Form_Field_Blocks extends Mage_Adminhtml_Block_System_Config_Form_Field {
    
    private function getStoreId() {
        
        $storeId = 0;
        
        if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level
        {
            $storeId = Mage::getModel('core/store')->load($code)->getId();
        }
        elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
        {
            $websiteId = Mage::getModel('core/website')->load($code)->getId();
            $storeId = Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId();
        }
        
        return $storeId;
    }
    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        
        $url = $this->getUrl('owlthemes_admin/install/blocks', array("store_id" => $this->getStoreId()));
        $buttonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setType('button')
                        ->setClass('scalable')
                        ->setLabel('Install')
                        ->setOnClick("setLocation('$url')")
                        ->toHtml();
        return $buttonHtml;
    }
}