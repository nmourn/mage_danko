<?php
class OwlThemes_Danko_Block_Adminhtml_System_Config_Form_Field_Font_Google extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        
        $html = parent::_getElementHtml($element);

        $html .= $this->getPreviewElement($element->getHtmlId(), "Quick brown fox jumps over the lazy dog");
        
        return $html;
    }
    
    protected function getPreviewElement($selectId, $previewText) {
        
        $preview = '<br/><div class="gfont_preview_'.$selectId.'" style="font-size:20px; margin-top:10px;">' . 
        $previewText .
        '</div>
        <script>
             googleFontPreview = new googleFontPreviewModel("'.$selectId.'", "gfont_preview_'.$selectId.'");
        </script>
        ';
        return $preview;
    }
}