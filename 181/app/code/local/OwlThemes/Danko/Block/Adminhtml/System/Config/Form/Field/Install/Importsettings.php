<?php
class OwlThemes_Danko_Block_Adminhtml_System_Config_Form_Field_Install_Importsettings extends Mage_Adminhtml_Block_System_Config_Form_Field {
    
    private function getStoreAndScope() {
        
        $storeId = 0;
        $scope = "default";
        
        if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level
        {
            $storeId = Mage::getModel('core/store')->load($code)->getId();
            $scope = "stores";
        }
        elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
        {
            $websiteId = Mage::getModel('core/website')->load($code)->getId();
            $storeId = Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId();
            $scope = "websites";
        }
        
        return array("scope" => $scope, "store_id" => $storeId);
    }

    protected function _createButton($label, $url) {
        $buttonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setType('button')
                        ->setClass('scalable')
                        ->setLabel($label)
                        ->setOnClick("setLocation('$url')")
                        ->toHtml();
        return $buttonHtml;
    }
    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {

        $demos = array("clothes", "food", "electronics");
        $buttons = array();
        $params = $this->getStoreAndScope();
        for ($i = 0; $i < count($demos); $i++) {
            $demoParam = array("demo" => $demos[$i]);
            $url = $this->getUrl('owlthemes_admin/install/settings', array_merge($params, $demoParam));
            $label = sprintf("Demo #%d: (%s)", $i+1, $demos[$i]);
            $buttons[] = $this->_createButton($label, $url);
        }
        return implode("<br /><br />", $buttons);
        
    }
}