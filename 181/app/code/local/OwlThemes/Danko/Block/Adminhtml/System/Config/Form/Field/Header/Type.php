<?php
class OwlThemes_Danko_Block_Adminhtml_System_Config_Form_Field_Header_Type extends Mage_Adminhtml_Block_System_Config_Form_Field {
    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        
        $html = parent::_getElementHtml($element);
        $script = '
        <script>
            configTransformer.init({imgs: ["http://nvm-magento.com/danko/media/owlthemes/danko/cms/amex.jpg", 
                                            "http://nvm-magento.com/danko/media/owlthemes/danko/cms/master.jpg",
                                            "http://nvm-magento.com/danko/media/owlthemes/danko/cms/visa.jpg",
                                            "http://nvm-magento.com/danko/media/owlthemes/danko/cms/paypal.jpg"],
                selectId: "danko_settings_header_header_type"});
            configTransformer.transform();
        </script>';
        return $html . "\n" . $script;
    }
}