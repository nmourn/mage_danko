<?php
class OwlThemes_Danko_Block_Cms_Wysiwyg_Image_Chooser extends Mage_Adminhtml_Block_Template {

    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element) {

        $config = $this->getConfig();
        $chooseBtn = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable btn-chooser')
            ->setLabel($config['button']['open'])
            ->setOnClick('MediabrowserUtility.openDialog(\''.$this->getUrl('*/cms_wysiwyg_images/index', array('target_element_id' => $element->getId())).'\')')
            ->setDisabled($element->getReadOnly());

        $text = new Varien_Data_Form_Element_Text();
        $text->setForm($element->getForm())
             ->setId($element->getId())
             ->setName($element->getName())
             ->setClass('widget-option input-text');
        if ($element->getRequired()) {
            $text->addClass('required-entry');
        }
        if ($element->getValue()) {
            $text->setValue($element->getValue());
        }
        $element->setData('after_element_html', $text->getElementHtml().$chooseBtn->toHtml());

        return $element;
    }
}