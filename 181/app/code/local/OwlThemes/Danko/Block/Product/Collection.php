<?php
class OwlThemes_Danko_Block_Product_Collection extends Mage_Catalog_Block_Product_Abstract {

    const DEFAULT_PRODUCTS_COUNT = 10;

    protected $_productsCount;
    protected $_productCollection;
    
    protected $categoryFilter;
    protected $isNew;
    protected $isSale;
    protected $isFeatured;
    protected $orderBy;
    protected $orderDirection;
    protected $count;
    protected $isRandom;
    

    protected function _construct() {

        parent::_construct();
        $this->processSettings();
    }
    
    /**
     * Set category filter from given string of category ids
     * @param string $ids
     * @return array
     */
    protected function setCategoryFilter($ids) {
        
        $this->categoryFilter = array();
        $ids = explode(",", $ids);
        for ($i = 0; $i < count($ids); $i++) {
            if ($val = intval($ids[$i])) {
                $this->categoryFilter[] = array('finset' => $val);
            }
        }
    }
    
    protected function processSettings() {
        
        $this->setCategoryFilter($this->getSetting('categories', ''));
        $this->isNew = $this->getSetting('new', false);
        $this->isSale = $this->getSetting('sale', false);
        $this->isFeatured = $this->getSetting('featured', false);
        $this->orderBy = $this->getSetting('order_by', 'entity_id');
        $this->orderDirection = $this->getSetting('order_dir', 'asc');
        $this->count = $this->getSetting('count', self::DEFAULT_PRODUCTS_COUNT);
        $this->isRandom = $this->getSetting('random', false);
    }
    
    protected function getSetting($key, $defaultValue) {
        
        return ($value = $this->getData($key)) ? $value : $defaultValue;
    }
    

    public function _getProductCollection() {

        if (is_null($this->_productCollection)) {
            $collection = Mage::getResourceModel('reports/product_collection');
            $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
            $collection = $this->_addProductAttributesAndPrices($collection)->addStoreFilter();
            if (count($this->categoryFilter) > 0) {
                $collection->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                           ->addAttributeToFilter('category_id', array($this->categoryFilter));
            }
            if ($this->isNew || $this->isSale) {
                $todayStartOfDayDate  = Mage::app()->getLocale()->date()
                    ->setTime('00:00:00')
                    ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        
                $todayEndOfDayDate  = Mage::app()->getLocale()->date()
                    ->setTime('23:59:59')
                    ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
            }
            if ($this->isNew) {
                $collection->addAttributeToFilter('news_from_date', array('or'=> array(
                    0 => array('date' => true, 'to' => $todayEndOfDayDate),
                    1 => array('is' => new Zend_Db_Expr('null')))
                ), 'left')
                ->addAttributeToFilter('news_to_date', array('or'=> array(
                    0 => array('date' => true, 'from' => $todayStartOfDayDate),
                    1 => array('is' => new Zend_Db_Expr('null')))
                ), 'left')
                ->addAttributeToFilter(
                    array(
                        array('attribute' => 'news_from_date', 'is'=>new Zend_Db_Expr('not null')),
                        array('attribute' => 'news_to_date', 'is'=>new Zend_Db_Expr('not null'))
                        )
                  );
            }
            if ($this->isSale) {
                $collection->addAttributeToFilter('special_from_date', array('or'=> array(
                        0 => array('date' => true, 'to' => $todayEndOfDayDate),
                        1 => array('is' => new Zend_Db_Expr('null')))
                ), 'left')
                ->addAttributeToFilter('special_to_date', array('or'=> array(
                        0 => array('date' => true, 'from' => $todayStartOfDayDate),
                        1 => array('is' => new Zend_Db_Expr('null')))
                ), 'left')
                ->addAttributeToFilter(
                        array(
                            array('attribute' => 'special_from_date', 'is'=>new Zend_Db_Expr('not null')),
                            array('attribute' => 'special_to_date', 'is'=>new Zend_Db_Expr('not null'))
                        )
                )
                ->addAttributeToFilter('special_price', array('is' => new Zend_Db_Expr('not null'))
                );
            }
            if ($this->isFeatured) {
                $collection->addAttributeToFilter('owlthemes_featured', array('eq' => 1));
            }
            if ($this->isRandom) {
                $collection->getSelect()->order('rand()');
            }
            if ($this->orderBy == 'ordered_qty') {
                $collection->addOrderedQty();
            }
            $collection->addAttributeToSelect('*')
                       ->setOrder($this->orderBy, $this->orderDirection)
                       ->setPageSize($this->count)->setCurPage(1);
            $collection->getSelect()->group('e.entity_id');
            $this->_productCollection = $collection;
        }

        return $this->_productCollection;
    }

    protected function _beforeToHtml() {

        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    public function setProductsCount($count) {

        $this->count = $count;
        return $this;
    }


    /*
    public function _toHtml() {

        echo "start collection";
        $c = $this->_getProductCollection()->load();
        foreach ($c as $p) {
            echo $p->getId();
        }
        echo "end of collection";
    }
    */

}