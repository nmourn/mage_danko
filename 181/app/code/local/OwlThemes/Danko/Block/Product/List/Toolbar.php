<?php
class OwlThemes_Danko_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar {

    protected $_theme;
    
    public function _construct() {
        
        parent::_construct();
        $this->_theme = $this->helper('danko');
        $this->_availableMode = array_merge($this->_availableMode, array('double' => $this->__('Double List')));
    }

    public function getCurrentMode() {

        $mode = $this->_getData('_current_grid_mode');
        if ($mode) {
            return $mode;
        }
        $modes = array_keys($this->_availableMode);
        $defaultMode = $this->_theme->getConf('category_page/default_mode');
        $mode = $this->getRequest()->getParam($this->getModeVarName());
        if ($mode) {
            if ($mode == $defaultMode) {
                Mage::getSingleton('catalog/session')->unsDisplayMode();
            } else {
                $this->_memorizeParam('display_mode', $mode);
            }
        } else {
            $mode = Mage::getSingleton('catalog/session')->getDisplayMode();
        }

        if (!$mode || !isset($this->_availableMode[$mode])) {
            $mode = $defaultMode;
        }
        $this->setData('_current_grid_mode', $mode);
        return $mode;
    }
}