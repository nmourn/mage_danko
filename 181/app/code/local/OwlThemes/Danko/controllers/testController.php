<?php
class OwlThemes_Danko_TestController extends Mage_Core_Controller_Front_Action {
    
    public function testAction() {
        
        $resp = array('a' => 1, 'b' => 2, 'c' => 3);
        $resp = Mage::helper('core')->jsonEncode($resp);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($resp);
    }
}