<?php
class OwlThemes_Danko_InstallController extends Mage_Adminhtml_Controller_Action {

    const REBUILD_IMAGE_PATHS = false;
    const GROUP_TO_STORE = false;

    protected function _loadFromFile($filename, $path="/cms") {

        $file = Mage::getModuleDir('etc', "OwlThemes_Danko") . $path  .DS . $filename;
        $data = simplexml_load_file($file);
        return $data;
    }

    /**
    * Get array of store view ids that are siblings of given storeId if GROUP_TO_STORE is true
    * otherwise wrap given storeId in array
    */
    protected function _getStores($storeId) {
        if (self::GROUP_TO_STORE && $storeId != 0) {
            $store = Mage::getModel('core/store')->load($storeId);
            $stores = $store->getGroup()->getStoreIds();
        }
        else {
            $stores = array($storeId); 
        }
        return $stores;
    }

    // Install CMS blocks and pages
    public function cmsAction() {

        $storeIdParam = $this->getRequest()->getParam("store_id", 0);
        $storeIds = $this->_getStores($storeIdParam);
        if (self::REBUILD_IMAGE_PATHS && $storeIdParam != 0) {
            $store = Mage::getModel('core/store')->load($storeIdParam);
            $storeName = strtolower($store->getGroup()->getName());
        }
        else 
            $storeName = "";
        $blockResults = $this->_installBlocks($storeIds, $storeName);
        $this->_addSessionNotice($blockResults, "block");
        $pageResults = $this->_installPages($storeIds, $storeName);
        $this->_addSessionNotice($pageResults, "page");

        $this->_redirectReferer();
    }

    public function settingsAction() {

        $storeId = $this->getRequest()->getParam("store_id", 0);
        $scope = $this->getRequest()->getParam("scope", "default");
        $demoName = $this->getRequest()->getParam("demo", "clothes");
        $allSettings = $this->_loadFromFile("settings.xml", "");
        $settings = $allSettings->$demoName->children();
        $config = Mage::getModel("core/config");
        foreach ($settings as $groupName => $group) {
            foreach ($group as $key => $value) {
                $path = sprintf("danko_settings/%s/%s", $groupName, $key);
                $config->saveConfig($path, (string) $value, $scope, $storeId);
            }
        }
        $this->_redirectReferer();
    }

    protected function _addSessionNotice($results, $type) {

        $session = Mage::getSingleton('core/session');
        if (count($results["succeeded"]) > 0) {
            $succeeded = implode(", ", $results["succeeded"]);
            $session->addSuccess("Following {$type}s have been successfuly installed: " . $succeeded);
        }
        
        if (count($results["failed"]) > 0) {
            $failed = implode(", ", $results["failed"]);
            $session->addNotice("Following ${type}s have been failed to install: " . $failed . "<br /> See log files for error text");
        }
    }

    /**
    * modify media image paths in given content 
    * and insert folder with store view name in it
    */
    protected function _modifyImgPaths($content, $storeName) {

        return preg_replace("/owlthemes\/danko\/(\w+)/", "owlthemes/danko/$1/$storeName", $content);
    }

    
    protected function _installBlocks($storeId, $storeName="") {
        
        
        $blocks = $this->_loadFromFile("cms_blocks.xml");
        $succeededBlocks = array();
        $failedBlocks = array();
        foreach ($blocks as $block) {
            
            $content = trim((string) $block->content);
            if ($storeName) {
                $content = $this->_modifyImgPaths($content, $storeName);
            }
            $newBlock = Mage::getModel('cms/block')
                    ->setTitle((string) $block->title)
                    ->setIdentifier((string) $block->identifier)
                    ->setContent($content)
                    ->setStores($storeId)
                    ->setActive(1);
            try {
                $newBlock->save();
                $succeededBlocks[] = (string) $newBlock->identifier;
            }
            
            catch (Exception $e) {
                $failedBlocks[] = (string) $newBlock->identifier;
                Mage::log("Failed to install " .  (string) $newBlock->identifier);
                Mage::log($e->getMessage());
            }            
        }
        return Array("succeeded" => $succeededBlocks, "failed" => $failedBlocks);
    }

    protected function _installPages() {

        $pages = $this->_loadFromFile("cms_pages.xml");
        $storeId = array($this->getRequest()->getParam("store_id", 0));

        $succeededPages = array();
        $failedPages = array();
        
        foreach ($pages as $page) {

            $content = trim((string) $page->content);
            if ($storeName) {
                $content = $this->_modifyImgPaths($content, $storeName);
            }
            $cmsPageData = array(
                    'title' => (string) $page->title,
                    'root_template' => (string) $page->template,
                    'meta_keywords' => (string) $page->meta_keywords,
                    'meta_description' => (string) $page->meta_description,
                    'identifier' => (string) $page->url_code,
                    'content_heading' => (string) $page->content_heading,
                    'stores' => $storeId,
                    'content' => $content
            );
            $newPage = Mage::getModel('cms/page')->setData($cmsPageData);
            try {
                $newPage->save();
                $succeededPages[] = (string) $newPage->identifier;
            }
            catch (Exception $e) {
                $failedPages[] = (string) $newPage->identifier;
                Mage::log("Failed to install " .  (string) $newPage->identifier);
                Mage::log($e->getMessage());
            }
        }
        return Array("succeeded" => $succeededPages, "failed" => $failedPages);
    }

    /**
    * Add given array to xmlElement
    */
    private function _arrayToXml($array, &$xmlElement) {

        foreach ($array as $key => $value) {

            $key = (is_numeric($key)) ? "key$key" : $key;
            if ($key == "content") {
                $child = $xmlElement->addChild($key);
                $this->_addCDATASection($value, $child);
            }
            else if (is_array($value)) {
                $child = $xmlElement->addChild($key);
                $this->_arrayToXml($value, $child);
            }
            else {
                $child = $xmlElement->addChild($key, $value);
            }
        }
    }


    private function _addCDATASection($text, &$xmlElement) {

        $node = dom_import_simplexml($xmlElement); 
        $no   = $node->ownerDocument; 
        $node->appendChild($no->createCDATASection($text));
    }

    public function getblocksAction() {

        $blocks = Mage::getModel('cms/block')->getCollection()
            ->addFieldToFilter('identifier', array('like'=>'danko'.'%'))
            ->addFieldToFilter('is_active', 1);
        $blocksArray = Array();
        foreach ($blocks as $block) {
            $blockElement = Array(
                "identifier" => $block->getIdentifier() ,
                "title" => $block->getTitle(),
                "content" => $block->getContent(),
            );
            $blocksArray[$block->getIdentifier()] = $blockElement;
        }
        $this->respondXml($blocksArray, "blocks");
    }

    public function getpagesAction() {

        $pageUrls = Array("danko-home", "danko-no-route", "about-us");
        $pagesArray = Array();
        foreach ($pageUrls as $pageUrl) {
            $page = Mage::getModel('cms/page')->load($pageUrl, 'identifier');
            $pageElement = Array(
                "url_code" => $pageUrl,
                "title" => $page->getTitle(),
                "template" => $page->getRootTemplate(),
                "meta_keywords" => $page->getMetaKeywords(),
                "meta_description" => $page->getMetaDescription(),
                "content_heading" => $page->getContentHeading(),
                "content" => $page->getContent(),
            );
            $pagesArray[$pageUrl] = $pageElement;
        }
        $this->respondXml($pagesArray, "pages");
    }

    // Respond with data represented in xml format
    public function respondXml($data, $rootNode="root") {

        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><$rootNode></$rootNode>");
        $this->getResponse()->setHeader('Content-type', 'text/xml', true);
        $this->_arrayToXml($data, $xml);
        $this->getResponse()->setBody($xml->asXML());
    }

    public function removeblocksAction() {

        $blocks = Mage::getModel('cms/block')->getCollection()
            ->addFieldToFilter('identifier', array('like'=>'danko'.'%'))
            ->addFieldToFilter('is_active', 1);
        foreach ($blocks as $block) {
            $block->delete();
        }
    }

    public function removepagesAction() {

        $pages = $this->_loadFromFile("cms_pages.xml");
        foreach ($pages as $page) {

            $page = Mage::getModel('cms/page')->load($page->url_code, 'identifier');
            $page->delete();
        }
    }
}