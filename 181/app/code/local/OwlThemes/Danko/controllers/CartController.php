<?php
require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

class OwlThemes_Danko_CartController extends Mage_Checkout_CartController {
    
    
    private function ajaxRespond($resp) {
        
        $resp = Mage::helper('core')->jsonEncode($resp);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($resp);
    }

    private function _getPrevReferer() {

        $prevReferer = Mage::getSingleton('core/session')->getData('prev_referer');
        if (!$prevReferer)
            $prevReferer = $this->_getRefererUrl();
        return $prevReferer;
    }
    
    public function addAction() {
        
        $this->loadLayout();
        $response = array();
        $referer = $this->_getPrevReferer();
        if ($referer) {
            Mage::getSingleton('core/session')->setData("prev_referer", $referer);
        }

        if (!$this->_validateFormKey()) {
            $response['status'] = 'Error';
            $response['error'] = $this->__('Invalid form key');
            $this->ajaxRespond($response);
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $response['status'] = 'Error';
                $response['error'] = $this->__('Product is not available');
                $this->ajaxRespond($response);
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);


            $response['status'] = 'Success';
            $response['top_cart'] = $this->getLayout('cms/index/index')->getBlock('topCart')->toHtml();
            $this->ajaxRespond($response);

        } catch (Exception $e) {

            $response['status'] = 'Error';
            $response['error'] = Mage::helper('core')->escapeHtml($e->getMessage());
            $this->ajaxRespond($response);

        }
    }
    
    public function optionsAction() {
        
        $productId  = (int) $this->getRequest()->getParam('product');
        
        // Prepare helper and params
        $viewHelper = Mage::helper('catalog/product_view');
        
        $params = new Varien_Object();
        $params->setCategoryId(false);
        $params->setSpecifyOptions(false);

        $referer = $this->_getRefererUrl();
        if ($referer) {
            Mage::getSingleton('core/session')->setData("prev_referer", $referer);
        }
        
        try {
            // For some reason calling viewHelper rendering doesn't work (404), 
            // therefore here is the code from that method
            $productHelper = Mage::helper('catalog/product');
            if (!$params) {
                $params = new Varien_Object();
            }

            
            $product = $productHelper->initProduct($productId, $this, $params);
            if (!$product) {
                throw new Mage_Core_Exception($this->__('Product is not loaded'), $this->ERR_NO_PRODUCT_LOADED);
            }

            $buyRequest = $params->getBuyRequest();
            if ($buyRequest) {
                $productHelper->prepareProductOptions($product, $buyRequest);
            }

            if ($params->hasConfigureMode()) {
                $product->setConfigureMode($params->getConfigureMode());
            }

            $update = $this->getLayout()->getUpdate();
            $update->addHandle('default');
            $this->addActionLayoutHandles();

            $update->addHandle('PRODUCT_TYPE_' . $product->getTypeId());
            $update->addHandle('PRODUCT_' . $product->getId());
            $this->loadLayoutUpdates();

            $this->generateLayoutXml()->generateLayoutBlocks();
            $this->initLayoutMessages(array('catalog/session', 'tag/session', 'checkout/session'))
                ->renderLayout();
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'options.log');
            if ($e->getCode() == $viewHelper->ERR_NO_PRODUCT_LOADED) {
                if (isset($_GET['store'])  && !$this->getResponse()->isRedirect()) {
                    $this->_redirect('');
                } elseif (!$this->getResponse()->isRedirect()) {
                    $this->_forward('noRoute');
                }
            } else {
                Mage::logException($e);
                $this->_forward('noRoute');
            }
        }
    }
    
    public function deleteAction()
    {

        $id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                ->save();
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
            }
        }
        $referer = $this->_getPrevReferer();
        if (preg_match("/owlthemes/", $referer)) {
            Mage::getSingleton('core/session')->unsData('prev_referer');
        }
        
        $this->getResponse()->setRedirect($referer);
    }
}